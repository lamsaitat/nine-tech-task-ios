//
//  CGSizeComparableTests.swift
//  NineTechTaskTests
//
//  Created by Sai Tat Lam on 16/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import XCTest
@testable import NineTechTask

class CGSizeComparableTests: XCTestCase {
    let size1 = CGSize(width: 10, height: 10)
    let size2 = CGSize(width: 50, height: 50)
    let size3 = CGSize(width: 100, height: 1)
    let size4 = CGSize(width: 50, height: 100)
    
    func testCompareLessOperator() {
        XCTAssertTrue(size1 < size2)
    }
    
    func testCompareGreaterOperator() {
        XCTAssertTrue(size2 > size1)
    }
    
    func testCompareLessThanOrEqualOperator() {
        XCTAssertTrue(size1 <= size1)
        
        XCTAssertTrue(size3 < size2)
    }
    
    func testCompareGreaterThanOrEqualOperator() {
        XCTAssertTrue(size2 >= size2)
    }
    
    /**
     Irregular case where one's width is longer and the other's height is taller.
     We then compare against their area.
     */
    func testLessOperatorInArea() {
        XCTAssertTrue(size2 <= size4)
    }
    
    /**
     Edge case of different width and height, but the areas are equal.
     In this case we are arbitrarily choosing left as "smaller".
     */
    func testCompareLessOperatorWithEqualArea() {
        XCTAssertFalse(size1 == size3)
        XCTAssertTrue(size1 < size3)
        XCTAssertTrue(size1 <= size3)
        XCTAssertTrue(size1 >= size3)
    }
}

//
//  ArticleCellViewModelTests.swift
//  NineTechTaskTests
//
//  Created by Sai Tat Lam on 19/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import XCTest
@testable import NineTechTask

class ArticleCellViewModelTests: BaseTestCase {
    var viewModel: ArticleCellViewModel?
    
    override func setUp() {
        viewModel = loadViewModelFromSampleArticle(sampleArticleURL())
    }
    
    func testInstantiationWithSampleData() {
        XCTAssertNotNil(viewModel, "Expects a valid sample article will successfully instantiate the view model.")
    }
    
    func testCellViewModelHeadlineDisplayStringWithTabletHeadline() {
        guard let viewModel = loadViewModelFromSampleArticle(sampleArticleWithValidTabletHeadlineURL()) else {
            return XCTFail("Unable to unwrap a valid article cell view model instance to run the test.")
        }
        
        if UI_USER_INTERFACE_IDIOM() == .phone {
            XCTAssertEqual(viewModel.headlineDisplayString, "Trump calls tariff war 'a little squabble'")
        } else if UI_USER_INTERFACE_IDIOM() == .pad {
            XCTAssertEqual(viewModel.headlineDisplayString, "This is an iPad headline")
        } else {
            // Do nothing.  Not building for non iPhone and iPad.
        }
    }
    
    /**
     Test sample 1: with overrides
     Test that the overrides will result in the headlineDisplayString to match with overrideHeadline.
     */
    func testCellViewModelHeadlineDisplayStringWithOverrides() {
        guard let viewModel = loadViewModelFromSampleArticle(sampleArticleURL()) else {
            return XCTFail("Unable to unwrap a valid article cell view model instance to run the test.")
        }
        XCTAssertEqual(viewModel.headlineDisplayString, viewModel.article.overrideHeadline)
        XCTAssertEqual(viewModel.headlineDisplayString, "<overrideHeadline> Trump calls tariff war 'a little squabble'")
    }
    func testCellViewModelTheAbstractDisplayStringWithOverrides() {
        guard let viewModel = loadViewModelFromSampleArticle(sampleArticleURL()) else {
            return XCTFail("Unable to unwrap a valid article cell view model instance to run the test.")
        }
        XCTAssertEqual(viewModel.theAbstractDisplayString, "<overrideAbstract> US President Donald Trump insisted that trade talks with China have not collapsed. Wall Street rallied in relief.")
    }
    
    /**
     Test sample 2: without overrides
     Test that when there is no overrides, the headlineDisplayString is expected to match with headline.
     */
    func testCellViewModelHeadlineDisplayStringWithoutOverrides() {
        guard let viewModel = loadViewModelFromSampleArticle(sampleArticle2OverridesAsNullURL()) else {
            return XCTFail("Unable to unwrap a valid article cell view model instance to run the test.")
        }
        XCTAssertEqual(viewModel.headlineDisplayString, "Labor loss to deliver a positive property 'shock'")
    }
    func testCellViewModelTheAbstractDisplayStringWithoutOverrides() {
        guard let viewModel = loadViewModelFromSampleArticle(sampleArticle2OverridesAsNullURL()) else {
            return XCTFail("Unable to unwrap a valid article cell view model instance to run the test.")
        }
        XCTAssertEqual(viewModel.theAbstractDisplayString, "Expect a rebound in Australia's property markets - that's the view from economists and industry following Labor's failure to win.")
    }
    
    /**
     Test sample 3: with empty overrides
     Test that the overrides with empty string values will result in the headlineDisplayString to match with headline.
     */
    func testCellViewModelHeadlineDisplayStringWithEmptyOverrides() {
        guard let viewModel = loadViewModelFromSampleArticle(sampleArticle2OverridesWithEmptyStringsURL()) else {
            return XCTFail("Unable to unwrap a valid article cell view model instance to run the test.")
            }
            XCTAssertEqual(viewModel.headlineDisplayString, "Labor loss to deliver a positive property 'shock'")
    }
    func testCellViewModelTheAbstractDisplayStringWithEmptyOverrides() {
        guard let viewModel = loadViewModelFromSampleArticle(sampleArticle2OverridesWithEmptyStringsURL()) else {
            return XCTFail("Unable to unwrap a valid article cell view model instance to run the test.")
        }
        XCTAssertEqual(viewModel.theAbstractDisplayString, "Expect a rebound in Australia's property markets - that's the view from economists and industry following Labor's failure to win.")
    }
    
    func testCellViewModelByLineDisplayString() {
        guard let viewModel = viewModel else {
            return XCTFail("Unable to unwrap a valid article cell view model instance to run the test.")
        }
        XCTAssertEqual(viewModel.byLineDisplayString, "Alexandra Alper and Susan Heavey")
    }
    
    func testLiveSignPostAttributedString() {
        guard let viewModel = viewModel else {
            return XCTFail("Unable to unwrap a valid article cell view model instance to run the test.")
        }
        
        let live = viewModel.liveSignPostAttributedString
        
        XCTAssertEqual(live.string, "● LIVE")
        let attrs = live.attributes(at: 0, effectiveRange: nil)
        let fontSize = CGFloat(14.0)
        let font = UIFont(name: "HelveticaNeue", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        XCTAssertEqual(attrs[NSAttributedString.Key.foregroundColor] as? UIColor, UIColor.red)
        XCTAssertEqual(attrs[NSAttributedString.Key.backgroundColor] as? UIColor, UIColor.clear)
        XCTAssertEqual(attrs[NSAttributedString.Key.font] as? UIFont, font)
    }
    
    func testTheAbstractDisplayAttributedStringFromSampleArticle() {
        guard let viewModel = viewModel else {
            return XCTFail("Unable to unwrap a valid article cell view model instance to run the test.")
        }
        
        let result = viewModel.theAbstractDisplayAttributedString(with: nil)
        XCTAssertEqual(result.string, "<overrideAbstract> US President Donald Trump insisted that trade talks with China have not collapsed. Wall Street rallied in relief.")
    }
    
    /**
     Live article sign post
     test case 1: Checks that a currently live article will display the live sign post.
     */
    func testTheAbstractDisplayAttributedStringFromLiveArticleCurrent() {
        viewModel = loadViewModelFromSampleLiveArticle(sampleLiveArticleLiveNowURL())
        guard let viewModel = viewModel else {
            return XCTFail("Unable to unwrap a valid article cell view model instance to run the test.")
        }
        
        let expected = "● LIVE The day started with Labor's announcement of a new summit between unions and businesses before the two major parties squared off over costings and speculated on Julie Bishop's future on the last day election ads are allowed to be broadcast. "
        
        let contentAttributes = [
            NSAttributedString.Key.backgroundColor: UIColor.clear,
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: UIFont.systemFontSize)
            ] as [NSAttributedString.Key: Any]
        let result = viewModel.theAbstractDisplayAttributedString(with: contentAttributes)
        XCTAssertEqual(result.string, expected)
        
        let theAbstractCharCount = viewModel.theAbstractDisplayString?.count ?? 0
        let contentSubstring = result.attributedSubstring(from: NSRange(location: expected.count - theAbstractCharCount, length: theAbstractCharCount))
        let resultAttributes = contentSubstring.attributes(at: 0, effectiveRange: nil)
        
        XCTAssertEqual(Set(resultAttributes.keys), Set(contentAttributes.keys))
        XCTAssertEqual(resultAttributes[NSAttributedString.Key.backgroundColor] as? UIColor, contentAttributes[NSAttributedString.Key.backgroundColor] as? UIColor)
        XCTAssertEqual(resultAttributes[NSAttributedString.Key.foregroundColor] as? UIColor, contentAttributes[NSAttributedString.Key.foregroundColor] as? UIColor)
        XCTAssertEqual(resultAttributes[NSAttributedString.Key.font] as? UIFont, contentAttributes[NSAttributedString.Key.font] as? UIFont)
    }
    
    /**
     Live article sign post
     test case 2: Checks that a past live article will NOT display the live sign post.
     */
    func testTheAbstractDisplayAttributedStringFromLiveArticlePast() {
        viewModel = loadViewModelFromSampleLiveArticle(sampleLiveArticlePastURL())
        guard let viewModel = viewModel else {
            return XCTFail("Unable to unwrap a valid article cell view model instance to run the test.")
        }
        
        let expected = "The day started with Labor's announcement of a new summit between unions and businesses before the two major parties squared off over costings and speculated on Julie Bishop's future on the last day election ads are allowed to be broadcast. "
        
        let contentAttributes = [
            NSAttributedString.Key.backgroundColor: UIColor.clear,
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: UIFont.systemFontSize)
        ] as [NSAttributedString.Key: Any]
        let result = viewModel.theAbstractDisplayAttributedString(with: contentAttributes)
        XCTAssertEqual(result.string, expected)
        let resultAttributes = result.attributes(at: 0, effectiveRange: nil)
        XCTAssertEqual(Set(resultAttributes.keys), Set(contentAttributes.keys))
        XCTAssertEqual(resultAttributes[NSAttributedString.Key.backgroundColor] as? UIColor, contentAttributes[NSAttributedString.Key.backgroundColor] as? UIColor)
        XCTAssertEqual(resultAttributes[NSAttributedString.Key.foregroundColor] as? UIColor, contentAttributes[NSAttributedString.Key.foregroundColor] as? UIColor)
        XCTAssertEqual(resultAttributes[NSAttributedString.Key.font] as? UIFont, contentAttributes[NSAttributedString.Key.font] as? UIFont)
    }
    
    func testCellViewModelThumbnailImageRemoteUrl() {
        guard let viewModel = viewModel else {
            return XCTFail("Unable to unwrap a valid article cell view model instance to run the test.")
        }
        XCTAssertEqual(viewModel.thumbnailImageRemoteUrl, URL(string: "https://www.fairfaxstatic.com.au/content/dam/images/h/1/e/e/s/a/image.related.thumbnail.375x250.p51ne3.13zzqx.png/1557895030045.jpg"))
    }
    
    /**
     Test the attributed string of the article sign post is defined correctly.
     */
    func testArticleSignPostAttributedString() {
        viewModel = loadViewModelFromSampleArticle(sampleArticleWithSignPostOpinionURL())
        guard let viewModel = viewModel else {
            return XCTFail("Unable to unwrap a valid article cell view model instance to run the test.")
        }
    
        let result = viewModel.signPostAttributedString
        XCTAssertNotNil(result)
        XCTAssertEqual(result?.string, " OPINION ")
        
        let fontSize = CGFloat(12.0)
        let font = UIFont(name: "HelveticaNeue", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        
        let attrs = result!.attributes(at: 0, effectiveRange: nil)
        XCTAssertEqual(attrs[NSAttributedString.Key.foregroundColor] as? UIColor, UIColor.white)
        XCTAssertEqual(attrs[NSAttributedString.Key.backgroundColor] as? UIColor, UIColor.afrBlue)
        XCTAssertEqual(attrs[NSAttributedString.Key.font] as? UIFont, font)
    }
    
    /**
     article with sign post OPINION
     test case 1: Checks that the sample aritcle will display sign post "OPINION".
     */
    func testTheAbstractDisplayAttributedStringWithSampleArticleSignPostOpinion() {
        viewModel = loadViewModelFromSampleArticle(sampleArticleWithSignPostOpinionURL())
        guard let viewModel = viewModel else {
            return XCTFail("Unable to unwrap a valid article cell view model instance to run the test.")
        }
        guard let signPost = viewModel.signPostAttributedString?.string else {
            return XCTFail("Unable to unwrap a valid sign post for this test.")
        }
        let expected = signPost + " " + "<overrideAbstract> US President Donald Trump insisted that trade talks with China have not collapsed. Wall Street rallied in relief."
        
        let contentAttributes = [
            NSAttributedString.Key.backgroundColor: UIColor.clear,
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: UIFont.systemFontSize)
            ] as [NSAttributedString.Key: Any]
        let result = viewModel.theAbstractDisplayAttributedString(with: contentAttributes)
        XCTAssertEqual(result.string, expected)
        
        let theAbstractCharCount = viewModel.theAbstractDisplayString?.count ?? 0
        let contentSubstring = result.attributedSubstring(from: NSRange(location: expected.count - theAbstractCharCount, length: theAbstractCharCount))
        let resultAttributes = contentSubstring.attributes(at: 0, effectiveRange: nil)
        
        XCTAssertEqual(Set(resultAttributes.keys), Set(contentAttributes.keys))
        XCTAssertEqual(resultAttributes[NSAttributedString.Key.backgroundColor] as? UIColor, contentAttributes[NSAttributedString.Key.backgroundColor] as? UIColor)
        XCTAssertEqual(resultAttributes[NSAttributedString.Key.foregroundColor] as? UIColor, contentAttributes[NSAttributedString.Key.foregroundColor] as? UIColor)
        XCTAssertEqual(resultAttributes[NSAttributedString.Key.font] as? UIFont, contentAttributes[NSAttributedString.Key.font] as? UIFont)
    }
}

//
//  AssetListViewModelTests.swift
//  NineTechTaskTests
//
//  Created by Sai Tat Lam on 18/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import NineTechTask

class AssetListViewModelTests: BaseTestCase {
    var viewModel: AssetListViewModel?
    
    override func setUp() {
        viewModel = AssetListViewModel()
    }
    
    /**
     Tests that the view model will successfully fetch the asset list, which the
     view model should update its assetList property.
     */
    func testFetchAssetList() {
        guard let viewModel = loadViewModelWithSampleAssetList() else {
            return XCTFail("Unable to instantiate an instance of AssetListViewModel to run the test.")
        }
        applyStubForFetchingSampleAssetListJsonSuccessfully()
        let expectation = XCTestExpectation(description: "Test AssetListViewModel for successful fetch for AssetList.")
        viewModel.fetchAssetList { (assetList, error) in
            XCTAssertNotNil(assetList)
            XCTAssertNil(error)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: TimeInterval(20))
        XCTAssertNotNil(viewModel.assetList)
    }
    
    /**
     Tests that if an instance of asset list is passed in via the constructor,
     it will populate the cellViewModels properly based on the matching article.
     */
    func testCellViewModelsCount() {
        guard let viewModel = loadViewModelWithSampleAssetList() else {
            return XCTFail("Unable to instantiate an instance of AssetListViewModel to run the test.")
        }
        
        XCTAssertEqual(viewModel.cellViewModels.count, 14)
    }
    
    /**
     Tests the private method populateCellViewModels() will populate 14 vms
     based on the 14 articles via didSet method.
     */
    func testPopulateCellViewModels() {
        guard let viewModel = viewModel else {
            return XCTFail("Unable to instantiate an instance of AssetListViewModel to run the test.")
        }
        guard let data = sampleAssetListData(),
            let list = try? JSONDecoder().decode(AssetList.self, from: data) else {
                return XCTFail("Unable to instantiate an instance of AssetList to run the test.")
        }
        
        XCTAssertNil(viewModel.assetList)
        viewModel.assetList = list
        XCTAssertEqual(viewModel.cellViewModels.count, viewModel.assetList?.assets.count)
        XCTAssertEqual(viewModel.cellViewModels.count, 14)
    }
    
    /**
     Checks the article list will display the most recently published article first.
     */
    func testPopulateCellViewModelsAreSortedLatestPublishedFirst() {
        guard let viewModel = viewModel else {
            return XCTFail("Unable to instantiate an instance of AssetListViewModel to run the test.")
        }
        guard let data = sampleAssetListData(),
            let list = try? JSONDecoder().decode(AssetList.self, from: data) else {
                return XCTFail("Unable to instantiate an instance of AssetList to run the test.")
        }
        
        XCTAssertNil(viewModel.assetList)
        viewModel.assetList = list
        XCTAssertEqual(viewModel.cellViewModels.count, viewModel.assetList?.assets.count)
        XCTAssertEqual(viewModel.cellViewModels.count, 14)
        
        // We already asserted that cellViewModels must not be empty.
        var timestamp = viewModel.cellViewModels.first!.article.timeStamp
        for (idx, cvm) in viewModel.cellViewModels.enumerated() {
            if idx == 0 {
                // Skips the first timestamp.
                continue
            }
            XCTAssertTrue(timestamp > cvm.article.timeStamp)
            // Updates the timestamp for the next round of comparison.
            timestamp = cvm.article.timeStamp
        }
    }

    /**
     Tests the view model's display strings matches with the asset list's displayName.
     */
    func testTitleDisplayString() {
        guard let viewModel = loadViewModelWithSampleAssetList() else {
            return XCTFail("Unable to instantiate an instance of AssetListViewModel to run the test.")
        }
        XCTAssertEqual(viewModel.titleDisplayString, "AFR iPad Editor's Choice")
    }
    
    /**
     Tests the view model will correctly return the expected rows based on the cell view models count.
     */
    func testNumberOfRowsInSection() {
        guard let viewModel = loadViewModelWithSampleAssetList() else {
            return XCTFail("Unable to instantiate an instance of AssetListViewModel to run the test.")
        }
        XCTAssertEqual(viewModel.numberOfRows(inSection: 0), viewModel.cellViewModels.count)
        XCTAssertEqual(viewModel.numberOfRows(inSection: 0), 14)
    }
    
    /**
     Makes sure the view model will give the correct cell view model at any valid indexPath.
     */
    func testRetrievingCellViewModelAtIndexPath() {
        guard let viewModel = loadViewModelWithSampleAssetList() else {
            return XCTFail("Unable to instantiate an instance of AssetListViewModel to run the test.")
        }
        for row in 0..<viewModel.cellViewModels.count {
            let indexPath = IndexPath(row: row, section: 0)
            let result = viewModel.cellViewModel(at: indexPath)
            let expected = viewModel.cellViewModels[row]
            XCTAssertNotNil(result)
            XCTAssertEqual(result?.article.id, expected.article.id)
        }
        
        let outOfBoundIndexPath1 = IndexPath(row: 999, section: 0)
        XCTAssertNil(viewModel.cellViewModel(at: outOfBoundIndexPath1))
        let outOfBoundIndexPath2 = IndexPath(row: 0, section: 10)
        XCTAssertNil(viewModel.cellViewModel(at: outOfBoundIndexPath2))
    }
}

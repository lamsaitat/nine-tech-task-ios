//
//  AssetListViewControllerTests.swift
//  NineTechTaskTests
//
//  Created by Sai Tat Lam on 18/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import XCTest
@testable import NineTechTask

class AssetListViewControllerTests: BaseTestCase {
    var viewController: AssetListViewController?
    
    override func setUp() {
        guard let vc = loadViewControllerFromStoryboard() else {
            return XCTFail("Failed to instantiate an instance of AssetListViewController from Main storyboard.")
        }
        viewController = vc
    }

    /**
     Checks the view controller is configured properly.
     */
    func testViewControllerInstantiatesProperly() {
        guard let viewController = loadViewControllerFromStoryboard() else {
            return XCTFail("Failed to instantiate an instance of AssetListViewController from Main storyboard.")
        }
        viewController.loadViewIfNeeded()
        XCTAssertNotNil(viewController)
        XCTAssertNotNil(viewController.tableView)
        XCTAssertNotNil(viewController.viewModel)
    }
    
    func testRefreshDataSuccess() {
        guard let viewController = viewController else {
            return XCTFail("Cannot unwrap the view controller to run the test.")
        }
        applyStubForFetchingSampleAssetListJsonSuccessfully()
        
        viewController.loadViewIfNeeded()
        UIApplication.shared.keyWindow?.rootViewController = viewController.navigationController
        
        let expectation = XCTestExpectation(description: "Wait for modal present to complete and test.")
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            expectation.fulfill()
        }
        viewController.refreshData()
        wait(for: [expectation], timeout: 5)
        XCTAssertNotNil(viewController.viewModel.assetList)
        XCTAssertEqual(viewController.title, "AFR iPad Editor's Choice")
    }
    
    /**
     Tests the models will not be reloaded from a failed API fetch with 500 error.
     Expects an alert to display.
     */
    func testReloadModelsFailedWith500Error() {
        guard let viewController = viewController else {
            return XCTFail("Cannot unwrap the view controller to run the test.")
        }
        viewController.loadViewIfNeeded()
        UIApplication.shared.keyWindow?.rootViewController = viewController.navigationController
        
        applyStubForFetchingSampleAssetListJsonWithError()
        
        let expectation = XCTestExpectation(description: "Wait for modal present to complete and test.")
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            expectation.fulfill()
        }
        viewController.refreshData()
        wait(for: [expectation], timeout: 3)
        XCTAssertNotNil(viewController.navigationController?.presentedViewController)
        XCTAssertTrue(viewController.navigationController?.presentedViewController is UIAlertController)
    }
    
    /**
     Make sure the setupView perform correctly.
     Expects a UIRefreshControl to be created and assigned to the table view.
     */
    func testSetupViews() {
        guard let viewController = viewController else {
            return XCTFail("Cannot unwrap the view controller to run the test.")
        }
        viewController.loadViewIfNeeded()
        
        viewController.setupViews()
        XCTAssertNotNil(viewController.tableView.refreshControl, "Expects the refresh control to be created.")
    }
    
    /**
     Test the controller is able to display an alert view signalling an error.
     */
    func testDisplayErrorAlert() {
        guard let viewController = viewController else {
            return XCTFail("Cannot unwrap the view controller to run the test.")
        }
        viewController.loadViewIfNeeded()
        UIApplication.shared.keyWindow?.rootViewController = viewController.navigationController
        let expectation = XCTestExpectation(description: "Wait for modal present to complete and test.")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            expectation.fulfill()
        }
        
        let alert = viewController.displayErrorAlert(URLError(.badServerResponse), retry: nil)
        XCTAssertNotNil(alert)
        wait(for: [expectation], timeout: 3)
        XCTAssertEqual(alert.actions.count, 1)
        XCTAssertEqual(alert.presentingViewController, viewController.navigationController)
    }
    
    /**
     Tests the error alert will display retry when a block is supplied.
     */
    func testDisplayErrorAlertWithRetry() {
        guard let viewController = viewController else {
            return XCTFail("Cannot unwrap the view controller to run the test.")
        }
        viewController.loadViewIfNeeded()
        UIApplication.shared.keyWindow?.rootViewController = viewController.navigationController
        let expectation = XCTestExpectation(description: "Wait for modal present to complete and test.")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            expectation.fulfill()
        }
        
        let alert = viewController.displayErrorAlert(URLError(.badServerResponse), retry: {
            print("Retry tapped.")
        })
        XCTAssertNotNil(alert)
        wait(for: [expectation], timeout: 3)
        XCTAssertEqual(alert.actions.count, 2)
        XCTAssertEqual(alert.actions.first?.title, "Close")
        XCTAssertEqual(alert.actions.last?.title, "Retry")
        XCTAssertEqual(alert.presentingViewController, viewController.navigationController)
    }
    
    /**
     Tests that the views will be updated accordingly upon a successful data fetch.
     */
    func testRefreshViewsSuccessful() {
        guard let viewController = viewController else {
            return XCTFail("Cannot unwrap the view controller to run the test.")
        }
        applyStubForFetchingSampleAssetListJsonSuccessfully()
        
        viewController.loadViewIfNeeded()
        UIApplication.shared.keyWindow?.rootViewController = viewController.navigationController

        let expectation = XCTestExpectation(description: "Wait for modal present to complete and test.")

        viewController.viewModel.fetchAssetList { [weak viewController] (_, error) in
            DispatchQueue.main.async {
                if let error = error {
                    return XCTFail("Unexpected error from mock data fetch. Error: \(error.localizedDescription)")
                }
                viewController?.refreshViews()
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 5)
        XCTAssertEqual(viewController.title, "AFR iPad Editor's Choice")
    }
    
    /**
     Tests the table view should display the expected amount of items
     according to cell view models count.
     */
    func testNumberOfRows() {
        guard let viewController = viewController else {
            return XCTFail("Cannot unwrap the view controller to run the test.")
        }
        applyStubForFetchingSampleAssetListJsonSuccessfully()
        
        viewController.loadViewIfNeeded()
        XCTAssertEqual(viewController.tableView(viewController.tableView, numberOfRowsInSection: 0), 0)
        
        let expectation = XCTestExpectation(description: "Wait for modal present to complete and test.")
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            expectation.fulfill()
        }
        viewController.viewModel.fetchAssetList { [weak viewController] (_, error) in
            DispatchQueue.main.async {
                if let error = error {
                    return XCTFail("Unexpected error from mock data fetch. Error: \(error.localizedDescription)")
                }
                viewController?.refreshViews()
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 5)
        XCTAssertEqual(viewController.tableView(viewController.tableView, numberOfRowsInSection: 0), 14)
    }
    
    /**
     Tests the functionality of configure(cell, viewModel) method.
     Expects the correct label text for
     1. headline
     2. byLine
     3. theAbstract
     Also simulates an async test to ensure that the cell is able to download
     and attach the article's thumbnail image to the thumbnailImageView.
     */
    func testConfigureArticleCell() {
        guard let viewController = viewController else {
            return XCTFail("Cannot unwrap the view controller to run the test.")
        }
        guard let data = sampleArticleData(sampleArticleURL()), let article = try? JSONDecoder().decode(Article.self, from: data) else {
            return XCTFail("Unable to instantiate an instance of the sample article to run tests.")
        }
        applyStubForFetchingSampleImage()
        let vm = ArticleCellViewModel(with: article)
        viewController.loadViewIfNeeded()
        
        guard let cell = viewController.tableView.dequeueReusableCell(withIdentifier: AssetListViewController.CellIdentifier.articleCellStandard.rawValue, for: IndexPath(row: 0, section: 0)) as? ArticleCell else {
            return XCTFail("Failed to create an article cell from table view.")
        }
        
        // Checks initial state.
        XCTAssertEqual(cell.thumbnailHeightConstraint.constant, 0)
        XCTAssertEqual(cell.thumbnailLeadingSpaceConstraint.constant, 0)
        
        let expectation = XCTestExpectation(description: "Waiting for image fetch.")
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            expectation.fulfill()
        }
        
        viewController.configure(cell, with: vm)
        
        wait(for: [expectation], timeout: 5.0)
        
        XCTAssertEqual(cell.headlineLabel.text, "<overrideHeadline> Trump calls tariff war 'a little squabble'")
        XCTAssertEqual(cell.byLineLabel.text, "Alexandra Alper and Susan Heavey")
        XCTAssertEqual(cell.theAbstractLabel.text, "<overrideAbstract> US President Donald Trump insisted that trade talks with China have not collapsed. Wall Street rallied in relief.")
        
        XCTAssertNotNil(cell.thumbnailImageView.image)
        XCTAssertEqual(cell.thumbnailHeightConstraint.constant, cell.expandedThumbnailHeight, "Expects a thumbnail applied will have non-zero height constraint.")
        XCTAssertEqual(cell.thumbnailLeadingSpaceConstraint.constant, cell.expandedThumbnailLeading, "Expects a thumbnail applied will have non-zero leading space padding.")
    }
    
    /**
     Tests that the AssetListViewController is able to push to display an
     articleViewController with the selected article via segue.
     WARNING: This test at times has been problematic when running in a test
     suite along with other tests (running the entire suite in this test class.)
     I'm suspecting that between each test within this class, the application is not relaunched to a clean state.
     */
    func testViewControllerSelectArticleAndPushSegue() {
        guard let viewController = viewController else {
            return XCTFail("Unable to unwrap a valid view controller to run the test.")
        }
        UIApplication.shared.keyWindow?.rootViewController = viewController.navigationController
        applyStubForFetchingSampleAssetListJsonSuccessfully()
        applyStubForFetchingSampleImage()
        viewController.loadViewIfNeeded()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            viewController.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)
            viewController.performSegue(withIdentifier: AssetListViewController.SegueIdentifier.pushToArticle.rawValue, sender: nil)
        }
        
        let expectation = XCTestExpectation(description: "Wait for segue push to complete and test.")
        DispatchQueue.main.asyncAfter(deadline: .now() + 6) {
            let articleViewController = viewController.navigationController?.topViewController as? ArticleViewController
            XCTAssertNotNil(articleViewController)
            XCTAssertNotNil(articleViewController?.article)
            XCTAssertNotNil(articleViewController?.webView.url)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10)
        
        // This assert is deemed invalid as the fact that the resulting web view url can be different due to redirect or http -> https (which is another redirect).
        //        XCTAssertEqual(articleViewController?.article?.url, articleViewController?.webView.url)
    }
}


// MARK: - Helper methods
extension AssetListViewControllerTests {
    func loadViewControllerFromStoryboard() -> AssetListViewController? {
        guard let vc = mainStoryboard()?.instantiateViewController(withIdentifier: AssetListViewController.StoryboardIdentifier.default.rawValue) as? AssetListViewController else {
            return nil
        }
        _ = UINavigationController(rootViewController: vc)
        return vc
    }
}

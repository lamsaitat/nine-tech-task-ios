//
//  ArticleViewControllerTests.swift
//  NineTechTaskTests
//
//  Created by Sai Tat Lam on 19/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import NineTechTask

class ArticleViewControllerTests: BaseTestCase {
    var viewController: ArticleViewController?
    
    override func setUp() {
        guard let vc = mainStoryboard()?.instantiateViewController(withIdentifier: ArticleViewController.StoryboardId.default.rawValue) as? ArticleViewController else {
            return XCTFail("Unable to instantiate an ArticleViewController from storyboard.")
        }
        viewController = vc
    }

    func testViewControllerWillInstantiateProperlyFromStoryboard() {
        guard let viewController = viewController else {
            return XCTFail("Unable to unwrap a valid view controller to run the test.")
        }
        viewController.loadViewIfNeeded()
        XCTAssertNotNil(viewController.webView)
    }
    
    /**
     Ensures that the web view has loaded the sample html.
     Note: We're not actually able to hit progress of 1.0 because it's a sample
     url with image assets missing in the bundle.  Anything not 0.0 should
     indicate that something has been loaded.
     */
    func testViewControllerWillLoadTheArticleUrlProperly() {
        guard let viewController = viewController else {
            return XCTFail("Unable to unwrap a valid view controller to run the test.")
        }
        guard let sampleArticle = loadViewModelFromSampleArticle(sampleArticleURL())?.article else {
            return XCTFail("Unable to load a sample article to run the test.")
        }
        applyStubForLoadingArticleWebpageSuccessfully(for: sampleArticle)
        viewController.loadViewIfNeeded()
        
        let expectation = XCTestExpectation(description: "Wait for html to finish loading to complete and test.")
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            expectation.fulfill()
        }
        viewController.article = sampleArticle
        viewController.loadArticle()
        wait(for: [expectation], timeout: 5)
        XCTAssertTrue(viewController.webView.estimatedProgress > 0.0)
    }
    
    /**
     Tests the functionality of presenting a UIActivityViewController for sharing.
     */
    func testViewControllerPresentShare() {
        guard let viewController = viewController else {
            return XCTFail("Unable to unwrap a valid view controller to run the test.")
        }
        guard let sampleArticle = loadViewModelFromSampleArticle(sampleArticleURL())?.article else {
            return XCTFail("Unable to load a sample article to run the test.")
        }
        applyStubForFetchingSampleImage()
        applyStubForLoadingArticleWebpageSuccessfully(for: sampleArticle)
        let navController = UINavigationController(rootViewController: viewController)
        UIApplication.shared.keyWindow?.rootViewController = navController
        viewController.article = sampleArticle
        viewController.loadViewIfNeeded()
        
        let expectation = XCTestExpectation(description: "Wait for segue push to complete and test.")
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            expectation.fulfill()
        }
        let vc = viewController.presentShareAction()
        
        wait(for: [expectation], timeout: 10)
        XCTAssertNotNil(vc)
    }
    
    /**
     Tests the functionality of presenting a UIActivityViewController for sharing,
     activated by calling action on the share bar button item itself.
     */
    func testViewControllerPresentShareViaButtonAction() {
        guard let viewController = viewController else {
            return XCTFail("Unable to unwrap a valid view controller to run the test.")
        }
        guard let sampleArticle = loadViewModelFromSampleArticle(sampleArticleURL())?.article else {
            return XCTFail("Unable to load a sample article to run the test.")
        }
        applyStubForFetchingSampleImage()
        applyStubForLoadingArticleWebpageSuccessfully(for: sampleArticle)
        let navController = UINavigationController(rootViewController: viewController)
        UIApplication.shared.keyWindow?.rootViewController = navController
        viewController.article = sampleArticle
        viewController.loadViewIfNeeded()
        
        let expectation = XCTestExpectation(description: "Wait for segue push to complete and test.")
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            expectation.fulfill()
        }
        guard let target = viewController.shareActionButtonItem.target, let action = viewController.shareActionButtonItem.action else {
            return XCTFail("Cannot unwrap the share button action to run the test.")
        }
        _ = target.perform(action, with: viewController.shareActionButtonItem)
        
        wait(for: [expectation], timeout: 10)
        XCTAssertNotNil(viewController.presentedViewController)
        XCTAssertTrue(viewController.presentedViewController is UIActivityViewController)
    }
}


// MARK: - Helper method
extension ArticleViewControllerTests {
    func sampleHtmlURL() -> URL? {
        return testBundle.url(forResource: "sample-article-page", withExtension: "html")
    }
    
    func applyStubForLoadingArticleWebpageSuccessfully(for article: Article) {
        guard let sampleHtmlUrl = sampleHtmlURL() else {
            return XCTFail("Unable to locate sample assetlist json for running the test.")
        }
        stub(condition: isAbsoluteURLString(article.url.absoluteString)) { _ -> OHHTTPStubsResponse in
            return fixture(filePath: sampleHtmlUrl.path, headers: ["Content-Type": "application/json"]).requestTime(1.0, responseTime: OHHTTPStubsDownloadSpeedWifi)
        }
    }
}

//
//  BaseTestCase.swift
//  NineTechTaskTests
//
//  Created by Sai Tat Lam on 16/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import NineTechTask

/**
 The BaseTestCase class.
 Here hosts some of the common code that will be used across most of the test files.
 */
class BaseTestCase: XCTestCase {
    lazy var testBundle: Bundle = {
        return Bundle(for: type(of: self))
    }()
    
    /* A standard date formatter following the GMT representation of time.
     Used to validate UNIX time conversion to Date object, since the JSON's
     timestamp is POSIX time is actually accurate to milliseconds (which is not
     standard as UNIX time is only to seconds.)
     See https://www.epochconverter.com/
     */
    lazy var timeStampDateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "EEEE, d MMMM yyyy HH:mm:ss.SSS"
        df.timeZone = TimeZone(secondsFromGMT: 0)
        return df
    }()
    
    func sampleAssetListURL() -> URL? {
        guard let url = testBundle.url(forResource: "sample-assetlist", withExtension: "json") else {
            XCTFail("Unable to locate a sample asset list json for conducting tests.")
            return nil
        }
        return url
    }
    
    func sampleAssetListData() -> Data? {
        guard let url = sampleAssetListURL(), let data = try? Data(contentsOf: url) else {
            XCTFail("Unable to load the sample asset list json for conducting tests.")
            return nil
        }
        return data
    }
    
    func sampleImageURL() -> URL? {
        guard let url = testBundle.url(forResource: "sample-image", withExtension: "json") else {
            XCTFail("Unable to load the sample image json for conduction tests.")
            return nil
        }
        return url
    }
    
    func sampleImageData() -> Data? {
        guard let url = sampleImageURL(), let data = try? Data(contentsOf: url) else {
            XCTFail("Unable to load the sample image json into memory.")
            return nil
        }
        return data
    }
    
    func randomSampleImageURL() -> URL? {
        let number = Int.random(in: 0...1)
        let filename = "sample-image-\(number)"
        guard let url = testBundle.url(forResource: filename, withExtension: "jpg") else {
            XCTFail("Unable to locate sample image: \(filename).jpg")
            return nil
        }
        return url
    }
    
    func mainStoryboard() -> UIStoryboard? {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}


// MARK: - Sample Article data
extension BaseTestCase {
    func sampleArticleData(_ url: URL? = nil) -> Data? {
        guard let url = url, let data = try? Data(contentsOf: url) else {
            XCTFail("Unable to load the sample article json for conducting tests.")
            return nil
        }
        return data
    }
    
    func sampleArticleURL() -> URL? {
        guard let url = testBundle.url(forResource: "sample-article", withExtension: "json") else {
            XCTFail("Unable to locate a sample article json for conducting tests.")
            return nil
        }
        return url
    }
    
    func sampleArticleWithSignPostOpinionURL() -> URL? {
        guard let url = testBundle.url(forResource: "sample-article-opinion", withExtension: "json") else {
            XCTFail("Unable to locate a sample article json for conducting tests.")
            return nil
        }
        return url
    }
    
    func sampleArticleWithValidTabletHeadlineURL() -> URL? {
        guard let url = testBundle.url(forResource: "sample-article-tabletHeadline", withExtension: "json") else {
            XCTFail("Unable to locate a sample article json for conducting tests.")
            return nil
        }
        return url
    }
    
    func sampleArticleWithNullTabletHeadlineURL() -> URL? {
        guard let url = testBundle.url(forResource: "sample-article-null-tabletHeadline", withExtension: "json") else {
            XCTFail("Unable to locate a sample article json for conducting tests.")
            return nil
        }
        return url
    }
    
    func sampleArticle2OverridesAsNullURL() -> URL? {
        guard let url = testBundle.url(forResource: "sample-article2-overrides-as-null", withExtension: "json") else {
            XCTFail("Unable to locate a sample article json for conducting tests.")
            return nil
        }
        return url
    }
    
    func sampleArticle2OverridesWithNullKeysURL() -> URL? {
        guard let url = testBundle.url(forResource: "sample-article2-overrides-null-headlines", withExtension: "json") else {
            XCTFail("Unable to locate a sample article json for conducting tests.")
            return nil
        }
        return url
    }
    
    func sampleArticle2OverridesWithEmptyStringsURL() -> URL? {
        guard let url = testBundle.url(forResource: "sample-article2-overrides-empty-strings", withExtension: "json") else {
            XCTFail("Unable to locate a sample article json for conducting tests.")
            return nil
        }
        return url
    }
    
    // MARK: - Live article
    func sampleLiveArticlePastURL() -> URL? {
        guard let url = testBundle.url(forResource: "sample-livearticle-past", withExtension: "json") else {
            XCTFail("Unable to locate a sample live article json for conducting tests.")
            return nil
        }
        return url
    }
    func sampleLiveArticleLiveNowURL() -> URL? {
        guard let url = testBundle.url(forResource: "sample-livearticle-live", withExtension: "json") else {
            XCTFail("Unable to locate a sample live article json for conducting tests.")
            return nil
        }
        return url
    }
}


// MARK: - Convenient factory methods
extension BaseTestCase {
    /**
     Loads an instance of a 'valid' sample AssetList and pack it into a ViewModel instance.
     */
    func loadViewModelWithSampleAssetList() -> AssetListViewModel? {
        guard let data = sampleAssetListData(),
            let list = try? JSONDecoder().decode(AssetList.self, from: data) else {
                XCTFail("Unable to instantiate an instance of AssetList to run the test.")
                return nil
        }
        return AssetListViewModel(with: list)
    }
    
    /**
     Loads an instance of a 'valid' sample Article and pack it into a ViewModel instance.
     */
    func loadViewModelFromSampleArticle(_ url: URL? = nil) -> ArticleCellViewModel? {
        guard let url = url, let data = sampleArticleData(url), let article = try? JSONDecoder().decode(Article.self, from: data) else {
            XCTFail("Unable to instantiate an instance of the sample article to run tests.")
            return nil
        }
        
        return ArticleCellViewModel(with: article)
    }
    
    /**
     Loads an instance of a 'valid' sample live Article and pack it into a ViewModel instance.
     */
    func loadViewModelFromSampleLiveArticle(_ url: URL?) -> ArticleCellViewModel? {
        guard let article = try? sampleLiveArticle(from: url) else {
            XCTFail("Unable to instantiate an instance of the sample article to run tests.")
            return nil
        }
        
        return ArticleCellViewModel(with: article)
    }
    
    func sampleLiveArticle(from url: URL?) throws -> LiveArticle? {
        guard let url = url, let data = sampleArticleData(url) else {
            return nil
        }
        return try JSONDecoder().decode(LiveArticle.self, from: data)
    }
    
    func sampleArticle(from url: URL?) throws -> Article? {
        guard let url = url, let data = sampleArticleData(url) else {
            return nil
        }
        return try JSONDecoder().decode(Article.self, from: data)
    }
}


// MARK: - OHHTTPStubs network mocking.
extension BaseTestCase {
    func applyStubForFetchingSampleAssetListJsonSuccessfully() {
        guard let sampleJsonUrl = sampleAssetListURL() else {
            return XCTFail("Unable to locate sample assetlist json for running the test.")
        }
        let filePath = sampleJsonUrl.path
        let urlString = API.EndPoint.assetList.url().absoluteString
        
        stub(condition: isAbsoluteURLString(urlString)) { _ -> OHHTTPStubsResponse in
            return fixture(filePath: filePath, headers: ["Content-Type": "application/json"]).requestTime(1.0, responseTime: OHHTTPStubsDownloadSpeedWifi)
        }
    }
    
    func applyStubForFetchingSampleAssetListJsonWithError() {
        stub(condition: isAbsoluteURLString(API.EndPoint.assetList.url().absoluteString)) { _ -> OHHTTPStubsResponse in
            let error = URLError(.badServerResponse)
            return OHHTTPStubsResponse(error: error)
        }
    }
    
    /**
     Mocks network request with test bundled sample image for a specific URL, or all types of jpg if none supplied.
     */
    func applyStubForFetchingSampleImage(_ imageUrl: URL? = nil) {
        guard let randomSampleImageUrl = randomSampleImageURL() else {
            return XCTFail("Unable to locate a sample image for running the test.")
        }
        
        stub(condition: isExtension("jpg")) { _ -> OHHTTPStubsResponse in
            return fixture(filePath: randomSampleImageUrl.path, headers: ["Content-Type": "image/jpeg"]).requestTime(1.0, responseTime: OHHTTPStubsDownloadSpeedWifi)
        }
    }
}

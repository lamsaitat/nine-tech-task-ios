//
//  AssetListTests.swift
//  NineTechTaskTests
//
//  Created by Sai Tat Lam on 16/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import XCTest
@testable import NineTechTask

class AssetListTests: BaseTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    /**
     To test the AssetList object can be instantiated from the sample json.
     */
    func testInstantiatingAssetListFromSampleJson() {
        guard let data = sampleAssetListData() else {
            return XCTFail("Unable to load the sample json into memory for running the test.")
        }
        
        do {
            let list = try JSONDecoder().decode(AssetList.self, from: data)
            XCTAssertNotNil(list)
            
            XCTAssertEqual(list.id, 67184313)
            XCTAssertEqual(list.displayName, "AFR iPad Editor's Choice")
            
            let df = timeStampDateFormatter
            XCTAssertEqual(list.timeStamp, df.date(from: "Wednesday, 15 May 2019 04:37:07.523"))
            XCTAssertEqual(list.lastModified, df.date(from: "Wednesday, 15 May 2019 04:37:10.045"))
            XCTAssertNotNil(list.url)
            XCTAssertEqual(list.url, URL(string: "/content/dam/lists/1/3/z/z/q/x/list.html")!)
            XCTAssertEqual(list.assets.count, 14)
            
            let liveArticles = list.assets.filter {
                return $0 is LiveArticle
            }
            XCTAssertEqual(liveArticles.count, 2)
        } catch let error {
            XCTFail("Failed to instantiate AssetList instance due to error: \(error.localizedDescription)")
        }
    }
    
    /**
     This test makes sure that an invalid URL from asset's base initialiser will
     throw nothing but an invalidURL error.
     Any cases other than catching a Asset.CodingError.invalidUrl should fail
     the test, and passes otherwise.
     */
    func testInvalidUrlInAssetListFromBadSampleJson() {
        guard let url = testBundle.url(forResource: "sample-badassetlist", withExtension: "json"),
            let data = try? Data(contentsOf: url) else {
            return XCTFail("Unable to load the sample json into memory for running the test.")
        }
        
        do {
            let list = try JSONDecoder().decode(AssetList.self, from: data)
            XCTAssertNil(list, "Test does not expect an AssetList instance to successfully instantiate.")
        } catch let error as Asset.CodingError {
            XCTAssertEqual(error, Asset.CodingError.invalidUrl)
        } catch let error {
            XCTFail("Unexpected error: \(error.localizedDescription)")
        }
    }
}

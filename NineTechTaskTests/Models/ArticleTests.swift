//
//  ArticleTests.swift
//  NineTechTaskTests
//
//  Created by Sai Tat Lam on 16/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import XCTest
@testable import NineTechTask

class ArticleTests: BaseTestCase {
    var article: Article!
    
    override func setUp() {
        article = try? sampleArticle(from: sampleArticleURL())
        guard article != nil else {
            return XCTFail("Unable to decode an article instance from sample json for running tests.")
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    /**
     Tests that the article can be instantiated from a sample article.json,
     which is extracted from the sample-assetlist.json
     Expects a valid Article instance.
     */
    func testInstantiateArticle() {
        guard let data = sampleArticleData(sampleArticleURL()) else {
            return XCTFail("Unable to load the sample article json into memory for running the test.")
        }
        do {
            let article = try JSONDecoder().decode(Article.self, from: data)
            XCTAssertNotNil(article)
            
            XCTAssertEqual(article.id, 1520129451)
            XCTAssertEqual(article.assetType, "ARTICLE")
            
            let df = timeStampDateFormatter
            let expectedTimeStamp = df.date(from: "Tuesday, 14 May 2019 17:19:46.000")
            let expectedLastModified = df.date(from: "Tuesday, 14 May 2019 17:20:58.208")
            XCTAssertEqual(article.timeStamp, expectedTimeStamp)
            XCTAssertEqual(article.lastModified, expectedLastModified)
            XCTAssertNotNil(article.url)
            XCTAssertEqual(article.url, URL(string: "http://www.afr.com/news/world/north-america/trump-calls-tariff-war-a-little-squabble-20190514-p51ne3"))
            
            // Article specific.
            XCTAssertEqual(article.headline, "Trump calls tariff war 'a little squabble'")
            XCTAssertEqual(article.theAbstract, "US President Donald Trump insisted that trade talks with China have not collapsed. Wall Street rallied in relief.")
            XCTAssertEqual(article.byLine, "Alexandra Alper and Susan Heavey")
            XCTAssertEqual(article.relatedImages.count, 6)
            
            // Check Article overrides
            XCTAssertNotNil(article.overrideHeadline)
            XCTAssertEqual(article.overrideHeadline, "<overrideHeadline> Trump calls tariff war 'a little squabble'")
            XCTAssertNotNil(article.overrideAbstract)
            XCTAssertEqual(article.overrideAbstract, "<overrideAbstract> US President Donald Trump insisted that trade talks with China have not collapsed. Wall Street rallied in relief.")
            
            XCTAssertNil(article.signPost)
            
            XCTAssertNotNil(article.tabletHeadline)
            XCTAssertEqual(article.tabletHeadline, "")
        } catch let error {
            XCTFail("Failed to decode the article instance with error: \(error.localizedDescription)")
        }
    }
    
    /**
     Test that the tabletHeadline is properly decoded into the article instance.
     */
    func testArticleWithNonEmptyTabletHeadline() {
        guard let data = sampleArticleData(sampleArticleWithValidTabletHeadlineURL()) else {
            return XCTFail("Unable to load the sample article json into memory for running the test.")
        }
        do {
            let article = try JSONDecoder().decode(Article.self, from: data)
            XCTAssertNotNil(article)
            
            XCTAssertEqual(article.id, 1520129451)
            XCTAssertEqual(article.assetType, "ARTICLE")
            
            XCTAssertNotNil(article.tabletHeadline)
            XCTAssertEqual(article.tabletHeadline, "This is an iPad headline")
        } catch let error {
            XCTFail("Failed to decode the article instance with error: \(error.localizedDescription)")
        }
    }
    
    /**
     Test that the tabletHeadline is properly decoded into the article instance.
     */
    func testArticleWithNullTabletHeadline() {
        guard let data = sampleArticleData(sampleArticleWithNullTabletHeadlineURL()) else {
            return XCTFail("Unable to load the sample article json into memory for running the test.")
        }
        do {
            let article = try JSONDecoder().decode(Article.self, from: data)
            XCTAssertNotNil(article)
            
            XCTAssertEqual(article.id, 1520129451)
            XCTAssertEqual(article.assetType, "ARTICLE")
            
            XCTAssertNil(article.tabletHeadline)
        } catch let error {
            XCTFail("Failed to decode the article instance with error: \(error.localizedDescription)")
        }
    }
    
    /**
     Overrides null case #1
     Tests that in situations like a null "overrides" sub-json will still return a valid instance.
     */
    func testArticleToInstantiateInNullOverridesScenario() {
        guard let data = sampleArticleData(sampleArticle2OverridesAsNullURL()) else {
            return XCTFail("Unable to load the sample article json into memory for running the test.")
        }
        do {
            let article = try JSONDecoder().decode(Article.self, from: data)
            XCTAssertNotNil(article)
            
            XCTAssertEqual(article.id, 1520131398)
            XCTAssertEqual(article.assetType, "ARTICLE")
            
            let df = timeStampDateFormatter
            let expectedTimeStamp = df.date(from: "Sunday, 19 May 2019 10:04:22.644")
            let expectedLastModified = df.date(from: "Sunday, 19 May 2019 10:04:23.930")
            XCTAssertEqual(article.timeStamp, expectedTimeStamp)
            XCTAssertEqual(article.lastModified, expectedLastModified)
            XCTAssertNotNil(article.url)
            XCTAssertEqual(article.url, URL(string: "http://www.afr.com/news/economy/labor-loss-to-deliver-a-positive-property-shock-20190518-p51ow6"))
            
            // Article specific.
            XCTAssertEqual(article.headline, "Labor loss to deliver a positive property 'shock'")
            XCTAssertEqual(article.theAbstract, "Expect a rebound in Australia's property markets - that's the view from economists and industry following Labor's failure to win.")
            XCTAssertEqual(article.byLine, "Matthew Cranston")
            XCTAssertEqual(article.relatedImages.count, 5)
            
            // Check Article overrides
            XCTAssertNil(article.overrideHeadline)
            XCTAssertNil(article.overrideAbstract)
        } catch let error {
            XCTFail("Failed to decode the article instance with error: \(error.localizedDescription)")
        }
    }
    
    /**
     Overrides null case #2
     Tests that in situations like overrides sub-json to contain null or missing keys will still return a valid instance.
     */
    func testArticleToInstantiateInOverridesNullKeysScenario() {
        guard let data = sampleArticleData(sampleArticle2OverridesWithNullKeysURL()) else {
            return XCTFail("Unable to load the sample article json into memory for running the test.")
        }
        do {
            let article = try JSONDecoder().decode(Article.self, from: data)
            XCTAssertNotNil(article)
            
            XCTAssertEqual(article.id, 1520131398)
            XCTAssertEqual(article.assetType, "ARTICLE")
            
            let df = timeStampDateFormatter
            let expectedTimeStamp = df.date(from: "Sunday, 19 May 2019 10:04:22.644")
            let expectedLastModified = df.date(from: "Sunday, 19 May 2019 10:04:23.930")
            XCTAssertEqual(article.timeStamp, expectedTimeStamp)
            XCTAssertEqual(article.lastModified, expectedLastModified)
            XCTAssertNotNil(article.url)
            XCTAssertEqual(article.url, URL(string: "http://www.afr.com/news/economy/labor-loss-to-deliver-a-positive-property-shock-20190518-p51ow6"))
            
            // Article specific.
            XCTAssertEqual(article.headline, "Labor loss to deliver a positive property 'shock'")
            XCTAssertEqual(article.theAbstract, "Expect a rebound in Australia's property markets - that's the view from economists and industry following Labor's failure to win.")
            XCTAssertEqual(article.byLine, "Matthew Cranston")
            XCTAssertEqual(article.relatedImages.count, 5)
            
            // Check Article overrides
            XCTAssertNil(article.overrideHeadline)
            XCTAssertNil(article.overrideAbstract)
        } catch let error {
            XCTFail("Failed to decode the article instance with error: \(error.localizedDescription)")
        }
    }
    
    /**
     Overrides edge case #3
     Tests that in situations like overrides sub-json to contain empty strings will still return a valid instance.
     */
    func testArticleToInstantiateInOverridesWithEmptyStringsScenario() {
        guard let data = sampleArticleData(sampleArticle2OverridesWithEmptyStringsURL()) else {
            return XCTFail("Unable to load the sample article json into memory for running the test.")
        }
        do {
            let article = try JSONDecoder().decode(Article.self, from: data)
            XCTAssertNotNil(article)
            
            XCTAssertEqual(article.id, 1520131398)
            XCTAssertEqual(article.assetType, "ARTICLE")
            
            let df = timeStampDateFormatter
            let expectedTimeStamp = df.date(from: "Sunday, 19 May 2019 10:04:22.644")
            let expectedLastModified = df.date(from: "Sunday, 19 May 2019 10:04:23.930")
            XCTAssertEqual(article.timeStamp, expectedTimeStamp)
            XCTAssertEqual(article.lastModified, expectedLastModified)
            XCTAssertNotNil(article.url)
            XCTAssertEqual(article.url, URL(string: "http://www.afr.com/news/economy/labor-loss-to-deliver-a-positive-property-shock-20190518-p51ow6"))
            
            // Article specific.
            XCTAssertEqual(article.headline, "Labor loss to deliver a positive property 'shock'")
            XCTAssertEqual(article.theAbstract, "Expect a rebound in Australia's property markets - that's the view from economists and industry following Labor's failure to win.")
            XCTAssertEqual(article.byLine, "Matthew Cranston")
            XCTAssertEqual(article.relatedImages.count, 5)
            
            // Check Article overrides
            XCTAssertNotNil(article.overrideHeadline)
            XCTAssertNotNil(article.overrideAbstract)
        } catch let error {
            XCTFail("Failed to decode the article instance with error: \(error.localizedDescription)")
        }
    }
    
    /**
     This test checks that the article is able to obtain the thumbnail image, or the smallest image according to the specs.
     */
    func testArticleToGetThumbnail() {
        guard let article = article else {
            return XCTFail("Unexpected nil article which is required to run the test.")
        }
        
        XCTAssertNotNil(article.thumbnail)
        XCTAssertEqual(article.thumbnail?.size, CGSize(width: 375, height: 250))
    }
    
    /**
     Tests that an article with signPost should be instantiated with a valid sign post.
     */
    func testInstantiateArticleWithSignPost() {
        guard let data = sampleArticleData(sampleArticleWithSignPostOpinionURL()) else {
            return XCTFail("Unable to load the sample article json into memory for running the test.")
        }
        do {
            let article = try JSONDecoder().decode(Article.self, from: data)
            XCTAssertNotNil(article)
            
            XCTAssertEqual(article.id, 1520129451)
            XCTAssertEqual(article.assetType, "ARTICLE")
            XCTAssertNotNil(article.signPost)
            XCTAssertEqual(article.signPost, "OPINION")
        } catch let error {
            XCTFail("Failed to decode the article instance with error: \(error.localizedDescription)")
        }
    }
}

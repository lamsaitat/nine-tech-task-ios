//
//  LiveArticleTests.swift
//  NineTechTaskTests
//
//  Created by Sai Tat Lam on 17/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import XCTest
@testable import NineTechTask

class LiveArticleTests: BaseTestCase {
    var article: LiveArticle!
    
    override func setUp() {
        article = try? sampleLiveArticle(from: sampleLiveArticleLiveNowURL())
        guard article != nil else {
            return XCTFail("Unable to decode a live article instance from sample json for running tests.")
        }
    }
    
    /**
     Tests that the sample-livearticle.json will instantiate a valid instance of LiveArticle.
     */
    func testLiveArticleInstantiation() {
        guard let data = sampleArticleData(sampleLiveArticleLiveNowURL()) else {
            return XCTFail("Unable to load the sample-livearticle.json into memory for running the test.")
        }
        
        do {
            let article = try JSONDecoder().decode(LiveArticle.self, from: data)
            XCTAssertNotNil(article)
            
            XCTAssertEqual(article.id, 1030277007)
            XCTAssertEqual(article.assetType, "LIVE_ARTICLE")
            
            let df = timeStampDateFormatter
            let expectedTimeStamp = df.date(from: "Wednesday, 15 May 2019 06:35:54.056")
            let expectedLastModified = df.date(from: "Wednesday, 15 May 2019 06:35:56.144")
            XCTAssertEqual(article.timeStamp, expectedTimeStamp)
            XCTAssertEqual(article.lastModified, expectedLastModified)
            XCTAssertNotNil(article.url)
            XCTAssertEqual(article.url, URL(string: "http://www.afr.com/news/politics/election/federal-election-2019-live-morrison-takes-fight-to-tasmania-20190514-h1eetr"))
            
            // Article specific.
            XCTAssertEqual(article.headline, "Federal Election 2019 Live: Adani stalling could back-fire on Shorten")
            XCTAssertEqual(article.theAbstract, "The day started with Labor's announcement of a new summit between unions and businesses before the two major parties squared off over costings and speculated on Julie Bishop's future on the last day election ads are allowed to be broadcast. ")
            XCTAssertEqual(article.byLine, "Lucas Baird")
            XCTAssertEqual(article.relatedImages.count, 1)
            XCTAssertTrue(article.live)
        } catch let error {
            XCTFail("Failed to decode the live article instance with error: \(error.localizedDescription)")
        }
    }
    
    /**
     Tests that the sample-livearticle-past.json will instantiate a valid instance of LiveArticle.
     Expects live == false.
     */
    func testPastLiveArticleInstantiation() {
        guard let data = sampleArticleData(sampleLiveArticlePastURL()) else {
            return XCTFail("Unable to load the sample-livearticle.json into memory for running the test.")
        }
        
        do {
            let article = try JSONDecoder().decode(LiveArticle.self, from: data)
            XCTAssertNotNil(article)
            XCTAssertFalse(article.live)
        } catch let error {
            XCTFail("Failed to decode the live article instance with error: \(error.localizedDescription)")
        }
    }
}

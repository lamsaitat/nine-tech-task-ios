//
//  ImageModelTests.swift
//  NineTechTaskTests
//
//  Created by Sai Tat Lam on 16/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import XCTest
@testable import NineTechTask

class ImageModelTests: BaseTestCase {
    var image: Image!
    
    override func setUp() {
        image = try? sampleImageFromJson()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testInstantiateImage() {
        guard let data = sampleImageData() else {
            return XCTFail("Unable to load the sample image json into memory for running the test.")
        }
        
        do {
            let image = try JSONDecoder().decode(Image.self, from: data)
            XCTAssertNotNil(image)
            
            XCTAssertEqual(image.id, 1030276954)
            XCTAssertEqual(image.assetType, "IMAGE")
            
            let df = timeStampDateFormatter
            XCTAssertEqual(image.timeStamp, df.date(from: "Tuesday, 14 May 2019 17:14:33.797"))
            XCTAssertEqual(image.lastModified, df.date(from: "Tuesday, 14 May 2019 17:14:33.797"))
            XCTAssertNotNil(image.url)
            XCTAssertEqual(image.url, URL(string: "https://www.fairfaxstatic.com.au/content/dam/images/h/1/e/e/s/a/image.related.wideLandscape.1500x844.p51ne3.13zzqx.png/1557895030045.jpg"))
            
            // Image specific
            XCTAssertEqual(image.description, "\"We're having a little squabble with China because we've been treated very unfairly for many, many decades,\" Trump said.")
            XCTAssertEqual(image.photographer, "Bloomberg")
            XCTAssertEqual(image.imageType, "wideLandscape")
            XCTAssertEqual(image.size, CGSize(width: 1500, height: 844))
        } catch let error {
            XCTFail("Failed to decode the Image instance with error: \(error.localizedDescription)")
        }
    }
}


// MARK: - Helper methods

extension ImageModelTests {
    func sampleImageFromJson() throws -> Image? {
        guard let data = sampleImageData() else {
            return nil
        }
        return try JSONDecoder().decode(Image.self, from: data)
    }
}

//
//  APITests.swift
//  NineTechTaskTests
//
//  Created by Sai Tat Lam on 17/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import XCTest
import Alamofire
import OHHTTPStubs

@testable import NineTechTask

/**
 All test cases here are mocked with stub network request so that we can test in
 a isolated, controlled environment.
 */
class MockAPITests: BaseTestCase {
    var api: API!
    
    override func setUp() {
        api = APIFactory.instance()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFetchAssetListSuccess() {
        guard let sampleJsonUrl = sampleAssetListURL() else {
            return XCTFail("Unable to locate sample assetlist json for running the test.")
        }
        let filePath = sampleJsonUrl.path
        let expectation = XCTestExpectation(description: "Test API.fetchAssetList for successful response.")
        let urlString = API.EndPoint.assetList.url().absoluteString
        
        stub(condition: isAbsoluteURLString(urlString)) { _ -> OHHTTPStubsResponse in
            return fixture(filePath: filePath, headers: ["Content-Type": "application/json"]).requestTime(0.0, responseTime: OHHTTPStubsDownloadSpeedWifi)
        }
        
        var expectedAssetList: AssetList?
        _ = api.fetchAssetList { (assetList, error) in
            XCTAssertNil(error)
            expectedAssetList = assetList
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: TimeInterval(20))
        XCTAssertNotNil(expectedAssetList)
        XCTAssertEqual(expectedAssetList?.id, 67184313)
        XCTAssertEqual(expectedAssetList?.assets.count, 14)
        
        let liveArticles = expectedAssetList?.assets.filter {
            return $0 is LiveArticle
        }
        XCTAssertEqual(liveArticles?.count, 2)
    }
    
    func testFetchAssetListAPICallFailed() {
        let urlString = API.EndPoint.assetList.url().absoluteString
        let expectation = XCTestExpectation(description: "Test API.fetchAssetList for 500 error.")
        
        stub(condition: isAbsoluteURLString(urlString)) { _ -> OHHTTPStubsResponse in
            let error = URLError(.badServerResponse)
            return OHHTTPStubsResponse(error: error)
        }
        var expectedError: Error?
        var list: AssetList?
        _ = api.fetchAssetList { (assetList, error) in
            list = assetList
            expectedError = error
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: TimeInterval(20))
        XCTAssertNil(list)
        XCTAssertNotNil(expectedError)
    }
    
    /**
     This test ensures that a malformed data in the json (invalid URL) should return error and no Asset List instance.
     */
    func testFetchAssetListAPICallMalformedJSONData() {
        guard let url = testBundle.url(forResource: "sample-badassetlist", withExtension: "json") else {
                return XCTFail("Unable to load the sample json into memory for running the test.")
        }
        let filePath = url.path
        let expectation = XCTestExpectation(description: "Test API.fetchAssetList for successful response but bad json content.")
        let urlString = API.EndPoint.assetList.url().absoluteString
        
        stub(condition: isAbsoluteURLString(urlString)) { _ -> OHHTTPStubsResponse in
            return fixture(filePath: filePath, headers: ["Content-Type": "application/json"]).requestTime(0.0, responseTime: OHHTTPStubsDownloadSpeedWifi)
        }
        
        var expectedError: Error?
        var list: AssetList?
        _ = api.fetchAssetList { (assetList, error) in
            list = assetList
            expectedError = error
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: TimeInterval(20))
        XCTAssertNil(list)
        XCTAssertNotNil(expectedError)
    }

    /**
     Test that the fetchAssetList API returning empty Data (or nil) should return
     an error with no AssetList instance.
     */
    func testFetchAssetListAPICallNilData() {
        let expectation = XCTestExpectation(description: "Test API.fetchAssetList for successful response.")
        let urlString = API.EndPoint.assetList.url().absoluteString
        
        stub(condition: isAbsoluteURLString(urlString)) { _ -> OHHTTPStubsResponse in
            return OHHTTPStubsResponse(data: Data(), statusCode: 200, headers: nil)
        }
        
        var expectedError: Error?
        var list: AssetList?
        _ = api.fetchAssetList { (assetList, error) in
            list = assetList
            expectedError = error
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: TimeInterval(20))
        XCTAssertNil(list)
        XCTAssertNotNil(expectedError)
    }
}

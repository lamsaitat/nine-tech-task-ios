
# iOS Tech task for Nine
### Written by Sai Tat Lam (Kelvin)

## Building the Project
1. This project makes use of Cocoapods, if you haven't installed Cocoapods on the build machine, please follow the instructions on https://cocoapods.org/
2. Install the latest [Xcode developer tools](https://developer.apple.com/xcode/downloads/) from Apple, this project is built using Xcode 10.2.1 (MacOS Mojave), using Swift 5.  Not tested with Xcode 10.1 due to Swift 5 is unsupported in Xcode 10.1.
3. Pull in the project dependencies:
```
# Open Terminal
# cd <project_root>
pod install
```
4. Open `NineTechTask.xcworkspace` in Xcode.
5. Build the `NineTechTask` scheme in Xcode.

## Third-party libraries
1. **Cocoapods** - Libraries and packages management
1. **SwiftLint** - I've included SwiftLint into the Podfile as a dependency.  The advantage is that it is directly integrated into the codebase to enforce a style-guide is in place.  This ensures consistent readability.
1. **Alamofire  + AlamofireImage** - A very well written and maintained URLSession wrapper framework.  The AlamofireImage provides UIImageView extensions to manage remote fetching with local caching functionality. 
1. **OHHTTPStubs** - Used for mocking network responses so we can perform unit-tests in isolated and consistent network environment directly with production ready API code.
1. **Swifter** - A small HTTP engine to run within the application.  This library is being used in the UI Testing target to act as a mock API server, the rest is same(-ish) as OHHTTPStubs.

## Architecture overview
The project makes use of MVVM where practical.
In AssetListViewController the AssetListViewModel further breaks down responsibilities by storing a list of ArticleCellViewModel dedicated for each Article in the list.  Sorting (latest timeStamp first) is done against cellViewModels property upon every population, which gets triggered on setting assetList.
I've implemented the AssetListViewController using UITableView due to the project spec requests for iPhone focused app.  If it requires iPad specific architecture, since most of the view logic and data logic are housed with AssetListViewModel and ArticleCellViewModel, we can easily reuse these VM classes in UICollectionViewController and UICollectionViewCell with minimal changes. 

One exception where I decided it is more practical to stick with MVC instead of MVVM is the ArticleViewController, due to the fact that the ArticleViewController only serve 2 purposes:
1. Displays a web view from the article
1. shares the article.

Using MVVM in this case will actually create more boilerplate, resulting in un-necessarily complicating the code.


## Testing - Unit Tests, etc.
The project is built using a Test Driven Development (TDD) approach.
The test cases are generally commented above the method name to explain the purpose of the test and what to expect of the result, with a few exceptions where the test cases are believed to be self explanatory enough from the method name.
This project also has some basic UI Testing written, combined with the use of Swifter (See Third Party Library section).

## Additional features
1. Pull to refresh - The landing screen of the app is a dynamic feed of articles from remote API endpoint.  I've implemented pull to refresh so that user can manually refresh the feed.
1. "LIVE" tag on article cell - Displays "LIVE" for live article that is "currently live".  Note that a live article that is no longer updated live (defined by the "live" property of type "LIVE_ARTICLE") will not display the "LIVE" tag.
1. "Sign post" tag on article cell - If the "signPost" property exist, displays the sign post (e.g. "EXCLUSIVE" or "OPINION") at the beginning of the abstract.
1. thumbnail image padding - automatically resizes the abstract label and cell height when a thumbnail image is ready to display on the cell.
1. Share button - I've placed a share button on the RHS of the navigation bar on ArticleViewController that presents the UIActivityViewController, a use case is as a user I would like to be able to share an interesting article that I enjoyed reading, or saving an article to the reading list of my favourite web browser app.

## Assumptions, considerations and other observations
1.  Image size comparison - The spec defines "If an ‘asset’ has ‘relatedImages’, display the smallest image available as a thumbnail on the cell", even though the Image entity itself actually has a "type": "thumbnail", honouring the spec I've decided to ignore this property altogether and went to compare image's width and height.  An image is considered "larger" than another if its width and height are both greater than the other.  However in the case of imageA has a greater width than imageB but imageB has a greater height than imageA, how do we than compare?  In my code, I've built the logic to compare it's pixel area, so if imageA(3, 4) = 12 is smaller than imageB(7, 2) = 14.  On a side note though, depending on image optimisation, am image of smaller pixel area does not guarantee smaller file size (a 100x100 of plain white image is likely to have smaller file size to 90x90 of the full 256-bit colour spectrum).  In an ideal situation I would opt to make use of the "type":"thumbnail" property or have image entity to specify the file size for comparison. 
1.  I had confirmed with Faiq that the headline and theAbstract's display logic is overrideHeadline (if present) > tabletHeadline(if present and iPad) > headline, and overrideAbstract (if present) > theAbstract
1.  iPhone vs iPad - The spec requests the project to be built for iPhone layout.  The project is configured as a universal app, and has been tested to run on iPad simulator as well.
1.  timeStamp - The timestamp is measured in milliseconds, the app has been configured to handle this.

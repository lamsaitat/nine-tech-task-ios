//
//  AssetListViewController.swift
//  NineTechTask
//
//  Created by Sai Tat Lam on 18/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import UIKit
import AlamofireImage

class AssetListViewController: UITableViewController {
    enum StoryboardIdentifier: String {
        case `default` = "AssetListViewController"
    }
    
    enum SegueIdentifier: String {
        case pushToArticle = "PushToArticleSegue"
    }
    
    enum CellIdentifier: String {
        case articleCellStandard = "ArticleCell"
    }
    
    let viewModel = AssetListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        tableView.refreshControl?.beginRefreshing()
        refreshData()
    }

    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(inSection: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.articleCellStandard.rawValue, for: indexPath) as? ArticleCell {
            let cvm = viewModel.cellViewModel(at: indexPath)
            configure(cell, with: cvm)
            
            // UITesting identifiers
            cell.accessibilityIdentifier = "ArticleCell_\(indexPath.row)"
            cell.headlineLabel.accessibilityIdentifier = "ArticleCell_headlineLabel_\(indexPath.row)"
            cell.byLineLabel.accessibilityIdentifier = "ArticleCell_byLineLabel_\(indexPath.row)"
            cell.theAbstractLabel.accessibilityIdentifier = "ArticleCell_theAbstractLabel_\(indexPath.row)"
            cell.thumbnailImageView.accessibilityIdentifier = "ArticleCell_thumbnailImageView_\(indexPath.row)"
            return cell
        } else {
            return UITableViewCell(style: .default, reuseIdentifier: "Cell")
        }
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueIdentifier.pushToArticle.rawValue,
            let vc = segue.destination as? ArticleViewController,
            let indexPath = tableView.indexPathForSelectedRow,
            let cvm = viewModel.cellViewModel(at: indexPath) {
            let article = cvm.article
            vc.article = article
        }
    }
}


// MARK: - Custom cells
extension AssetListViewController {
    func configure(_ cell: ArticleCell, with cellViewModel: ArticleCellViewModel?) {
        cell.headlineLabel.text = cellViewModel?.headlineDisplayString
        cell.byLineLabel.text = cellViewModel?.byLineDisplayString
        
        cell.theAbstractLabel.attributedText = cellViewModel?.theAbstractDisplayAttributedString(with: cell.theAbstractDefaultTextAttributes)
        cell.displayThumbnail(false)
        if let url = cellViewModel?.thumbnailImageRemoteUrl, let imageView = cell.thumbnailImageView {
            imageView.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(TimeInterval(0.5)), runImageTransitionIfCached: false) { response in
                if let error = response.error {
                    debugPrint("Error fetching image at url: \(String(describing: response.request?.url)) error: \(error.localizedDescription)")
                    return
                }
                self.tableView.beginUpdates()
                cell.displayThumbnail(true)
                self.tableView.endUpdates()
            }
        } else {
            cell.thumbnailImageView.image = nil
        }
    }
}

// MARK: - Data loading
extension AssetListViewController {
    @objc func refreshData() {
        viewModel.fetchAssetList { [weak self] (_, error) in
            DispatchQueue.main.async {
                // Safely unwrap self and abort if self has been released.
                guard let self = self else {
                    return
                }
                self.tableView.refreshControl?.endRefreshing()
                if let error = error {
                    _ = self.displayErrorAlert(error, retry: {
                        self.refreshData()
                    })
                    return
                }
                self.refreshViews()
            }
        }
    }
}


// MARK: - UI logic
extension AssetListViewController {
    func displayErrorAlert(_ error: Error, retry retryBlock: (() -> Void)?) -> UIAlertController {
        let message = "Unable to fetch articles.\n" +
        "Error: \(error.localizedDescription)"
        let alert = UIAlertController(title: "Sorry", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
        if let retryBlock = retryBlock {
            alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { _ in
                retryBlock()
            }))
        }
        present(alert, animated: true, completion: nil)
        return alert
    }
    
    func setupViews() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    func refreshViews() {
        title = viewModel.titleDisplayString
        tableView.reloadData()
    }
}

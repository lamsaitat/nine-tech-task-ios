//
//  AssetListViewModel.swift
//  NineTechTask
//
//  Created by Sai Tat Lam on 18/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import Foundation

class AssetListViewModel {
    let api = APIFactory.instance()
    var assetList: AssetList? {
        didSet {
            populateCellViewModels()
        }
    }
    var cellViewModels = [ArticleCellViewModel]()
    
    init(with assetList: AssetList? = nil) {
        self.assetList = assetList
        populateCellViewModels()
    }
}


// MARK: - Data fetching
extension AssetListViewModel {
    func fetchAssetList(completion: @escaping (_ assetList: AssetList?, _ error: Error?) -> Void) {
        _ = api.fetchAssetList { [weak self] (list, error) in
            self?.assetList = list
            completion(list, error)
        }
    }
    
    private func populateCellViewModels() {
        cellViewModels.removeAll()
        let cellVms = assetList?.assets.map({ article -> ArticleCellViewModel in
            return ArticleCellViewModel(with: article)
        }).sorted(by: {
            return $0.article.timeStamp > $1.article.timeStamp
        })
        if let cellVms = cellVms {
            cellViewModels.append(contentsOf: cellVms)
        }
    }
}


// MARK: - View Model data sources
extension AssetListViewModel {
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return cellViewModels.count
    }
    
    func cellViewModel(at indexPath: IndexPath) -> ArticleCellViewModel? {
        guard indexPath.section < numberOfSections, indexPath.row < numberOfRows(inSection: indexPath.section) else {
            return nil
        }
        return cellViewModels[indexPath.row]
    }
}


// MARK: - View Model view logics
extension AssetListViewModel {
    
    var titleDisplayString: String? {
        return assetList?.displayName
    }
}

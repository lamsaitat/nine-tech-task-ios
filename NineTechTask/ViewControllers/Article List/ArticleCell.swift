//
//  ArticleCell.swift
//  NineTechTask
//
//  Created by Sai Tat Lam on 19/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import UIKit

class ArticleCell: UITableViewCell {

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var headlineLabel: UILabel!
    @IBOutlet weak var byLineLabel: UILabel!
    @IBOutlet weak var theAbstractLabel: UILabel!

    @IBOutlet weak var thumbnailHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var thumbnailLeadingSpaceConstraint: NSLayoutConstraint!
    
    // Stores the value of expanded height so that we can re-apply at will.
    private(set) var expandedThumbnailHeight: CGFloat = 0
    private(set) var expandedThumbnailLeading: CGFloat = 0
    
    // Stores the default text label attributes on storyboard inflation so that we can re-apply them.
    private(set) var theAbstractDefaultTextAttributes = [NSAttributedString.Key: Any]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        expandedThumbnailHeight = thumbnailHeightConstraint.constant
        expandedThumbnailLeading = thumbnailLeadingSpaceConstraint.constant
        
        theAbstractDefaultTextAttributes = [
            NSAttributedString.Key.backgroundColor: theAbstractLabel.backgroundColor ?? UIColor.clear,
            NSAttributedString.Key.foregroundColor: theAbstractLabel.textColor ?? UIColor.black,
            NSAttributedString.Key.font: theAbstractLabel.font ?? UIFont.systemFont(ofSize: UIFont.systemFontSize)
        ]
        
        displayThumbnail(false) // Sets thumbnail to hide by default.
        updateConstraints()
    }
    
    func displayThumbnail(_ shouldDisplay: Bool) {
        thumbnailHeightConstraint.constant = shouldDisplay ? expandedThumbnailHeight : 0
        thumbnailLeadingSpaceConstraint.constant = shouldDisplay ? expandedThumbnailLeading : 0
    }
}

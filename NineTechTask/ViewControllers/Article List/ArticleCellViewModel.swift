//
//  ArticleCellViewModel.swift
//  NineTechTask
//
//  Created by Sai Tat Lam on 19/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import Foundation
import UIKit

class ArticleCellViewModel {
    let article: Article
    
    required init(with article: Article) {
        self.article = article
    }
}


// MARK: - View model display logic
extension ArticleCellViewModel {
    var headlineDisplayString: String? {
        if let overrideHeadline = article.overrideHeadline, overrideHeadline.isEmpty == false {
            return overrideHeadline
        } else if UI_USER_INTERFACE_IDIOM() == .pad, let tabletHeadline = article.tabletHeadline, tabletHeadline.isEmpty == false {
            return article.tabletHeadline
        }
        return article.headline
    }
    
    var byLineDisplayString: String? {
        return article.byLine
    }
    
    var theAbstractDisplayString: String? {
        if let overrideAbstract = article.overrideAbstract, overrideAbstract.isEmpty == false {
            return overrideAbstract
        }
        return article.theAbstract
    }
    
    func theAbstractDisplayAttributedString(with contentAttributes: [NSAttributedString.Key: Any]?) -> NSAttributedString {
        let result = NSMutableAttributedString()
        let whitespace = NSAttributedString(string: " ", attributes: contentAttributes)
        if let theAbstractDisplayString = theAbstractDisplayString {
            if let article = article as? LiveArticle, article.live == true {
                result.append(liveSignPostAttributedString)
                result.append(whitespace)
            } else if let signPost = signPostAttributedString {
                result.append(signPost)
                result.append(whitespace)
            }
            let content = NSAttributedString(string: theAbstractDisplayString, attributes: contentAttributes)
            result.append(content)
        }
        return result
    }
    
    var liveSignPostAttributedString: NSAttributedString {
        let fontSize = CGFloat(14.0)
        let font = UIFont(name: "HelveticaNeue", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        return NSAttributedString(string: "● LIVE", attributes: [
            NSAttributedString.Key.foregroundColor: UIColor.red,
            NSAttributedString.Key.backgroundColor: UIColor.clear,
            NSAttributedString.Key.font: font
        ])
    }
    
    var signPostAttributedString: NSAttributedString? {
        guard let signPost = article.signPost else {
            return nil
        }
        
        let fontSize = CGFloat(12.0)
        let font = UIFont(name: "HelveticaNeue", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        let attributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.backgroundColor: UIColor.afrBlue,
            NSAttributedString.Key.font: font
        ]
        return NSAttributedString(string: " \(signPost) ", attributes: attributes)
    }
    
    var thumbnailImageRemoteUrl: URL? {
        return article.thumbnail?.url
    }
}

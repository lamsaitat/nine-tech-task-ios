//
//  ArticleViewController.swift
//  NineTechTask
//
//  Created by Sai Tat Lam on 19/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import UIKit
import WebKit

class ArticleViewController: UIViewController {
    enum StoryboardId: String {
        case `default` = "ArticleViewController"
    }
    
    var article: Article? {
        didSet {
            loadArticle()
        }
    }
    
    @IBOutlet weak var shareActionButtonItem: UIBarButtonItem!
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadArticle()
    }
    
    func loadArticle() {
        guard let webView = webView else {
            return
        }
        guard let article = article else {
            debugPrint("WARNING: Article not assigned to ArticleViewController.")
            return
        }
        var request = URLRequest(url: article.url)
        request.cachePolicy = .reloadIgnoringLocalCacheData
        webView.load(request)
    }
}


// MARK: - IBAction
extension ArticleViewController {
    @IBAction func shareActionButtonItemTapped(_ sender: UIBarButtonItem) {
        _ = presentShareAction()
    }
}


// MARK: - public methods
extension ArticleViewController {
    func presentShareAction() -> UIActivityViewController? {
        guard let article = article else {
            return nil
        }
        let activityItems = [article.url] as [Any]
        
        let vc = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        if UI_USER_INTERFACE_IDIOM() == .pad {
            vc.popoverPresentationController?.barButtonItem = shareActionButtonItem
        }
        present(vc, animated: true, completion: nil)
        
        return vc
    }
}

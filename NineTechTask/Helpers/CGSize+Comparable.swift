//
//  CGSize+Comparable.swift
//  NineTechTask
//
//  Created by Sai Tat Lam on 16/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import Foundation
import UIKit

// MARK: - CGSize Comparable protocol

// Discussion: How exactly do we compare sizes? I can think of three scenarios,
// 1. if sizeA has longer width and height than sizeB
// 2. vice versa
// 3. sizeA has longer width but sizeB has taller height, in this case how
// should we determine?  I'm going to take the assumption to use smallest
// resolution, although it does not necessarily mean smallest in file size.
public func < (lhs: CGSize, rhs: CGSize) -> Bool {
    let lRect = CGRect(origin: .zero, size: lhs)
    let rRect = CGRect(origin: .zero, size: rhs)
    if lhs == rhs {
        return false
    } else if rRect.contains(lRect) {
        return true
    } else {
        let lhsArea = lhs.width * lhs.height
        let rhsArea = rhs.width * rhs.height
        if lhsArea == rhsArea {
            return true
        } else {
            return lhsArea < rhsArea
        }
    }
}

/**
 Overwrites the behaviour of <= because it is not as simple as negation of '>'.
 */
public func <= (lhs: CGSize, rhs: CGSize) -> Bool {
    return lhs < rhs || lhs == rhs
}

public func >= (lhs: CGSize, rhs: CGSize) -> Bool {
    return lhs > rhs || lhs == rhs
}

extension CGSize: Comparable {
}

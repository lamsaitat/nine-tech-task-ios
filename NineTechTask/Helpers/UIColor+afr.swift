//
//  UIColor+afr.swift
//  NineTechTask
//
//  Created by Sai Tat Lam on 20/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    open class var afrBlue: UIColor {
        return UIColor(red: CGFloat(57.0 / 255.0),
                       green: CGFloat(143.0 / 255.0),
                       blue: CGFloat(217.0 / 255.0),
                       alpha: 1.0)
    }
}

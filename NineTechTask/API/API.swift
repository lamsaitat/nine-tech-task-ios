//
//  API.swift
//  NineTechTask
//
//  Created by Sai Tat Lam on 17/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import Foundation

protocol API {
    typealias EndPoint = APIEndPoint
    typealias CompletionError = APICompletionError
    func fetchAssetList(completion: @escaping ((AssetList?, Error?) -> Void)) -> URLSessionTask?
}

class APIFactory {
    class func instance() -> API {
        return NineAPI()
    }
}

enum APIEndPoint: String {
    case assetList = "/1/coding_test/13ZZQX/full"
    
    private func baseUrl() -> URL {
        if ProcessInfo.processInfo.environment["USE_MOCK_SERVER"] == "YES" {
            guard let url = URL(string: "http://localhost:8080") else {
                fatalError()
            }
            return url
        } else {
            guard let url = URL(string: "https://bruce-v2-mob.fairfaxmedia.com.au") else {
                fatalError()
            }
            return url
        }
    }
    
    func url() -> URL {
        return baseUrl().appendingPathComponent(self.rawValue)
    }
}

enum APICompletionError: Error {
    case nilData
}

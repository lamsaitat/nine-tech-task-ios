//
//  NineAPI.swift
//  NineTechTask
//
//  Created by Sai Tat Lam on 17/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import Foundation
import Alamofire

class NineAPI: API {
    
    func fetchAssetList(completion: @escaping ((AssetList?, Error?) -> Void)) -> URLSessionTask? {
        let request = AF.request(EndPoint.assetList.url()).validate(statusCode: 200..<300).responseData { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            // It seems that if a Data instance is 0 byte or nil, AF will actually
            // already produce a ResponseSerializationFailureReason as an error.
            // We may not actually be able to hit the guard's failure scope.
            // However we still need to safely unwrap the optional as housekeeping.
            guard let data = response.data else {
                completion(nil, API.CompletionError.nilData)
                return
            }
            do {
                let assetList = try JSONDecoder().decode(AssetList.self, from: data)
                completion(assetList, nil)
            } catch let error {
                completion(nil, error)
            }
        }
        
        return request.task
    }
}

//
//  Image.swift
//  NineTechTask
//
//  Created by Sai Tat Lam on 16/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import Foundation
import UIKit

class Image: Asset {
    let description: String
    let imageType: String
    let photographer: String
    let size: CGSize
    
    private enum CodingKeys: String, CodingKey {
        case description
        case imageType = "type"
        case photographer
        case width
        case height
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.description = try values.decode(String.self, forKey: .description)
        self.imageType = try values.decode(String.self, forKey: .imageType)
        self.photographer = try values.decode(String.self, forKey: .photographer)
        
        let width = try values.decode(Int.self, forKey: .width)
        let height = try values.decode(Int.self, forKey: .height)
        self.size = CGSize(width: width, height: height)
        
        try super.init(from: decoder)
    }
}

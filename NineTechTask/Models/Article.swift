//
//  Article.swift
//  NineTechTask
//
//  Created by Sai Tat Lam on 16/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import Foundation
import UIKit

class Article: Asset {
    let headline: String
    let theAbstract: String
    let byLine: String
    let relatedImages: [Image]
    
    let tabletHeadline: String? // Used for tablet specific display.
    // Overrides
    let overrideHeadline: String?
    let overrideAbstract: String?
    
    lazy var thumbnail: Image? = {
        return relatedImages.reduce(nil, { (smallest, image) -> Image? in
            guard let smallest = smallest else {
                return image
            }
            return smallest.size < image.size ? smallest : image
        })
    }()
    
    private enum CodingKeys: String, CodingKey {
        case headline
        case theAbstract
        case byLine
        case relatedImages
        case overrides
        case tabletHeadline
    }
    
    private enum OverridesCodingKey: String, CodingKey {
        case overrideHeadline
        case overrideAbstract
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.headline = try values.decode(String.self, forKey: .headline)
        self.theAbstract = try values.decode(String.self, forKey: .theAbstract)
        self.byLine = try values.decode(String.self, forKey: .byLine)
        
        self.relatedImages = try values.decode([Image].self, forKey: .relatedImages)
        
        self.tabletHeadline = try values.decodeIfPresent(String.self, forKey: .tabletHeadline)
        
        if let overrides = try? values.nestedContainer(keyedBy: OverridesCodingKey.self, forKey: .overrides) {
            self.overrideHeadline = try overrides.decodeIfPresent(String.self, forKey: .overrideHeadline)
            self.overrideAbstract = try overrides.decodeIfPresent(String.self, forKey: .overrideAbstract)
        } else {
            self.overrideHeadline = nil
            self.overrideAbstract = nil
        }
        
        try super.init(from: decoder)
    }
}

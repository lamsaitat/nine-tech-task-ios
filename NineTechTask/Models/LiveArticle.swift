//
//  LiveArticle.swift
//  NineTechTask
//
//  Created by Sai Tat Lam on 17/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import Foundation

class LiveArticle: Article {
    let live: Bool
    
    private enum CodingKeys: String, CodingKey {
        case live
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        live = try values.decode(Bool.self, forKey: .live)
        try super.init(from: decoder)
    }
}

//
//  Asset.swift
//  NineTechTask
//
//  Created by Sai Tat Lam on 16/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import Foundation


class Asset: Decodable {
    let id: Int
    let assetType: String
    let timeStamp: Date
    let lastModified: Date
    let url: URL
    let signPost: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case assetType
        case timeStamp
        case lastModified
        case url
        case signPost
    }
    
    enum CodingError: Error {
        case malformedData
        case invalidUrl
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try values.decode(Int.self, forKey: .id)
        self.assetType = try values.decode(String.self, forKey: .assetType)
        
        // This is a non-standard UNIX time accurate to milliseconds.
        let timestamp = try values.decode(Int.self, forKey: .timeStamp)
        self.timeStamp = Date(timeIntervalSince1970: TimeInterval(timestamp) / 1000)
        
        // This is a non-standard UNIX time accurate to milliseconds.
        let lastModified = try values.decode(Int.self, forKey: .lastModified)
        self.lastModified = Date(timeIntervalSince1970: TimeInterval(lastModified) / 1000)
        
        // Custom decoding of urlstring into URL instance, in case of an invalid
        // URL we should throw an error to replicate the same behaviour like an
        // internal process.
        let urlString = try values.decode(String.self, forKey: .url)
        guard let url = URL(string: urlString) else {
            throw CodingError.invalidUrl
        }
        self.url = url
        
        self.signPost = try values.decodeIfPresent(String.self, forKey: .signPost)
    }
}

//
//  File.swift
//  NineTechTask
//
//  Created by Sai Tat Lam on 16/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import Foundation

class AssetList: Asset {
    let displayName: String
    let assets: [Article]
    
    private enum CodingKeys: String, CodingKey {
        case displayName
        case assets
    }
    
    private enum ArticleTypeKey: String, CodingKey {
        case assetType
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.displayName = try values.decode(String.self, forKey: .displayName)

        var articles = [Article]()
        // Parse the assets collection.
        var articlesContainer = try values.nestedUnkeyedContainer(forKey: .assets)
        var articlesContainerIterative = articlesContainer
        
        while articlesContainerIterative.isAtEnd == false {
            let articleContainer = try articlesContainerIterative.nestedContainer(keyedBy: ArticleTypeKey.self)
            let articleType = try articleContainer.decode(String.self, forKey: .assetType)
            
            switch articleType {
            case "LIVE_ARTICLE":
                let liveArticle = try articlesContainer.decode(LiveArticle.self)
                articles.append(liveArticle)
            case "ARTICLE":
                let article = try articlesContainer.decode(Article.self)
                articles.append(article)
            default:
                let article = try articlesContainer.decode(Article.self)
                articles.append(article)
            }
        }
        
        self.assets = articles
        try super.init(from: decoder)
    }
}

//
//  NineTechTaskUITests.swift
//  NineTechTaskUITests
//
//  Created by Sai Tat Lam on 20/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import XCTest
import Swifter

class NineTechTaskUITests: XCTestCase {
    
    var app: XCUIApplication = XCUIApplication()
    let stub = HTTPDynamicStubs()
    override func setUp() {

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        stub.setUp()
        app = XCUIApplication()
        app.launchEnvironment = ["USE_MOCK_SERVER": "YES"]
    }

    override func tearDown() {
        app.terminate()
        stub.tearDown()
    }

    /**
     Tests that the article list should load the correct elements.
     */
    func testLoadMainScreen() {
        // Explicitly launch the app to allow for test specific pre-launch setup.
        app.launch()
        // Test for correct title.
        let navBar = app.navigationBars["UI Testing Title"].otherElements["UI Testing Title"]
        XCTAssertTrue(navBar.exists)
        
        let tableView = app.tables.matching(identifier: "AssetListTableView")

        let rowIndex = 0
        let cell = tableView.cells.element(matching: .cell, identifier: "ArticleCell_\(rowIndex)")
        XCTAssertTrue(cell.exists)
        
        // Tests cell 0 - Regular article with overrides, no sign post.
        let headlineLabel = cell.staticTexts.matching(identifier: "ArticleCell_headlineLabel_\(rowIndex)").element
        let byLineLabel = cell.staticTexts.matching(identifier: "ArticleCell_byLineLabel_\(rowIndex)").element
        let theAbstractLabel = cell.staticTexts.matching(identifier: "ArticleCell_theAbstractLabel_\(rowIndex)").element
        let thumbnailImageView = cell.images.matching(identifier: "ArticleCell_thumbnailImageView_\(rowIndex)").element
        XCTAssertEqual(headlineLabel.label, "BREAKING: Trump calls tariff war 'a little squabble'")
        XCTAssertEqual(byLineLabel.label, "Alexandra Alper and Susan Heavey")
        XCTAssertEqual(theAbstractLabel.label, "BREAKING: US President Donald Trump insisted that trade talks with China have not collapsed. Wall Street rallied in relief.")
        XCTAssertTrue(thumbnailImageView.exists)
        
    }
    
    func testCurrentLiveCellOnAssetListScreen() {
        // Explicitly launch the app to allow for test specific pre-launch setup.
        app.launch()
        let tableView = app.tables.matching(identifier: "AssetListTableView")

        let rowIndex = 1
        
        let cell = tableView.cells.element(matching: .cell, identifier: "ArticleCell_\(rowIndex)")
        XCTAssertTrue(cell.exists)
        
        // Tests cell 1 - Current LIVE article cell.
        let headlineLabel = cell.staticTexts.matching(identifier: "ArticleCell_headlineLabel_\(rowIndex)").element
        let byLineLabel = cell.staticTexts.matching(identifier: "ArticleCell_byLineLabel_\(rowIndex)").element
        let theAbstractLabel = cell.staticTexts.matching(identifier: "ArticleCell_theAbstractLabel_\(rowIndex)").element
        let thumbnailImageView = cell.images.matching(identifier: "ArticleCell_thumbnailImageView_\(rowIndex)").element
        XCTAssertEqual(headlineLabel.label, "Adani stalling could back-fire on Shorten")
        XCTAssertEqual(byLineLabel.label, "Lucas Baird")
        XCTAssertEqual(theAbstractLabel.label, "● LIVE" + " " + "The day started with Labor's announcement of a new summit between unions and businesses before the two major parties squared off over costings and speculated on Julie Bishop's future on the last day election ads are allowed to be broadcast. ")
        XCTAssertTrue(thumbnailImageView.exists)
    }
    
    func testArticleCellWithOpinionSignPost() {
        // Explicitly launch the app to allow for test specific pre-launch setup.
        app.launch()
        asyncTest(expects: "Testing article cell with OPINION sign post", delay: 3) {
            let tableView = app.tables.matching(identifier: "AssetListTableView")
            
            let rowIndex = 2
            
            let cell = tableView.cells.element(matching: .cell, identifier: "ArticleCell_\(rowIndex)")
            XCTAssertTrue(cell.exists)
            
            // Tests cell 2 - Regular article with overrides, OPINION tag.
            let headlineLabel = cell.staticTexts.matching(identifier: "ArticleCell_headlineLabel_\(rowIndex)").element
            let byLineLabel = cell.staticTexts.matching(identifier: "ArticleCell_byLineLabel_\(rowIndex)").element
            let theAbstractLabel = cell.staticTexts.matching(identifier: "ArticleCell_theAbstractLabel_\(rowIndex)").element
            let thumbnailImageView = cell.images.matching(identifier: "ArticleCell_thumbnailImageView_\(rowIndex)").element
            XCTAssertEqual(headlineLabel.label, "This is an article with OPINION tag")
            XCTAssertEqual(byLineLabel.label, "Alexandra Alper and Susan Heavey")
            XCTAssertEqual(theAbstractLabel.label, " OPINION " + " " + "The override is very short with this one.")
            XCTAssertTrue(thumbnailImageView.exists)
        }
    }
    
    /**
     Tests that when a cell is unable to retrieve an image successfully, the image
     cell should have zero size.
     */
    func testImageNotFoundShouldNotDIsplayCellImage() {
        // Explicitly launch the app to allow for test specific pre-launch setup.
        app.launch()
        
        asyncTest(expects: "Wait the data fetching to be completed", delay: 3) {
            let tableView = app.tables.matching(identifier: "AssetListTableView")
            let rowIndex = 3
            
            let cell = tableView.cells.element(matching: .cell, identifier: "ArticleCell_\(rowIndex)")
            XCTAssertTrue(cell.exists)
            
            asyncTest(expects: "The image size should be zero", delay: 2) {
                let thumbnailImageView = cell.images.matching(identifier: "ArticleCell_thumbnailImageView_\(rowIndex)").element
                XCTAssertEqual(thumbnailImageView.frame.size, CGSize.zero)
            }
        }
    }
    
    /**
     Test the functionality of cell tapping.
     Tapping on a cell should display the article view with web view and a share button.
     */
    func testTappingCellShouldLaunchArticleViewController() {
        // Explicitly launch the app to allow for test specific pre-launch setup.
        app.launch()
        
        asyncTest(expects: "Wait the data fetching to be completed", delay: 3) {
            app.tables["AssetListTableView"].staticTexts["ArticleCell_headlineLabel_0"].tap()
            
            asyncTest(expects: "The webview should exist, and the share button.", delay: 3) {
                let webView = app.descendants(matching: .webView).element
                XCTAssertTrue(webView.exists)
                let shareButton = app.navigationBars.children(matching: .button).element(matching: .button, identifier: "ArticleShareButton")

                XCTAssertTrue(shareButton.exists)
            }
        }
    }
    
    /**
     Tests that when the API returns unsuccessful response, the screen should
     display an alert with Close and Retry buttons.
     */
    func testServerErrorToDisplayAlert() {
        stub.applyStubForAssetListFetching(statusCode: 500)
        
        // Explicitly launch the app to allow for test specific pre-launch setup.
        app.launch()
        
        asyncTest(expects: "In case of fetching fail, should display alert", delay: 3) {
            let alert = app.alerts.element(boundBy: 0)
            XCTAssertTrue(alert.exists)
            
            let closeButton = alert.buttons["Close"]
            XCTAssertTrue(closeButton.exists)
            let retryButton = alert.buttons["Retry"]
            XCTAssertTrue(retryButton.exists)
        }
    }
    
    /**
     Workflow test case
     This test is to simulate
     1. a successful fetch upon launch,
     2. then user drags to refresh and the fetch fails, thus displaying the error alert with prompts,
     3. user taps retry button, the alert should then dismiss and refresh,
     4. the fetch is successful once more and refreshes the screen.
     */
    func testRefreshFailedFlow() {
        app.launch()
        let rowIndex = 0
        asyncTest(expects: "Article list should fetch successfully here", delay: 2) {
            let tableView = app.tables.matching(identifier: "AssetListTableView")
            XCTAssertTrue(tableView.element.exists)
            
            let cell = tableView.cells.element(matching: .cell, identifier: "ArticleCell_\(rowIndex)")
            XCTAssertTrue(cell.exists)
            
            stub.applyStubForAssetListFetching(statusCode: 500)
            // drag to refresh...
            
            let start = cell.coordinate(withNormalizedOffset: CGVector.zero)
            let finish = cell.coordinate(withNormalizedOffset: CGVector(dx: 0, dy: 6))
            start.press(forDuration: 0, thenDragTo: finish)
            asyncTest(expects: "Article list should fail to fetch now.", delay: 3, testScope: {
                let alert = app.alerts.element(boundBy: 0)
                XCTAssertTrue(alert.exists)
                
                stub.applyStubForAssetListFetching(statusCode: 200)
                let retryButton = alert.buttons["Retry"]
                retryButton.tap()
                
                asyncTest(expects: "Article list should fetch successfully again", delay: 2) {
                    let alert = app.alerts.element(boundBy: 0)
                    XCTAssertFalse(alert.exists)
                    let cell = tableView.cells.element(matching: .cell, identifier: "ArticleCell_\(rowIndex)")
                    XCTAssertTrue(cell.exists)
                }
            })
        }
    }
    
    
    // MARK: - Helper methods
    
    func asyncTest(expects: String, delay: TimeInterval, testScope: () -> Void) {
        let expectation = XCTestExpectation(description: expects)
        // Give it some time to load...
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: delay * 5.0)
        testScope()
    }
}

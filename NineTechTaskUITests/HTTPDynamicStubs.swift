//
//  HTTPDynamicStubs.swift
//  NineTechTaskUITests
//
//  Created by Sai Tat Lam on 20/5/19.
//  Copyright © 2019 Sai Tat Lam. All rights reserved.
//

import Foundation
import Swifter

enum HTTPMethod {
    case POST
    case GET
}

class HTTPDynamicStubs {
    
    var server = HttpServer()
    
    func setUp() {
        setupInitialStubs()
        try? server.start()
    }
    
    func tearDown() {
        server.stop()
    }
    
    func setupInitialStubs() {
        // Setting up all the initial mocks from the array
//        for stub in initialStubs {
//            setupStub(stub.url, filename: stub.jsonFilename, method: stub.method)
//        }
        
        let testBundle = Bundle(for: type(of: self))
        if let fileUrl = testBundle.url(forResource: "sample-assetlist-success", withExtension: "json") {
            setupStub(remoteUrl: "/1/coding_test/13ZZQX/full", fileUrl: fileUrl, contentType: "application/json", method: .GET)
        }
        
        if let fileUrl = testBundle.url(forResource: "sample-image-0", withExtension: "jpg") {
            setupStub(remoteUrl: "/sample-image-0.jpg", fileUrl: fileUrl, contentType: "image/jpeg", method: .GET)
        }
        if let fileUrl = testBundle.url(forResource: "sample-image-1", withExtension: "jpg") {
            setupStub(remoteUrl: "/sample-image-1.jpg", fileUrl: fileUrl, contentType: "image/jpeg", method: .GET)
        }
        
        if let fileUrl = testBundle.url(forResource: "sample-article-page", withExtension: "html") {
            setupStub(remoteUrl: "/sample-article-page.html", fileUrl: fileUrl, contentType: "text/html", method: .GET)
        }
    }
    
    public func setupStub(_ remoteUrlString: String, filename: String, method: HTTPMethod = .GET) {
        let testBundle = Bundle(for: type(of: self))
        guard let fileUrl = testBundle.url(forResource: filename, withExtension: "json") else {
            debugPrint("Unable to locate response file.")
            return
        }
        setupStub(remoteUrl: remoteUrlString, fileUrl: fileUrl, method: method)
    }
    
    public func setupStub(remoteUrl: String, fileUrl: URL, contentType: String = "application/json", method: HTTPMethod = .GET) {
        
        guard let data = try? Data(contentsOf: fileUrl, options: .uncached) else {
            debugPrint("Unable to locate response file into memory.")
            return
        }
        // Looking for a file and converting it to JSON
//        let json = dataToJSON(data: data)
        
        // Swifter makes it very easy to create stubbed responses
        let response: ((HttpRequest) -> HttpResponse) = { _ in
            
            switch contentType {
            case "application/json":
                return HttpResponse.ok(.json(self.dataToJSON(data: data) as AnyObject))
            case "text/html":
                return HttpResponse.ok(.html(String(data: data, encoding: .utf8)!))
            default:
                return HttpResponse.ok(.data(data))
            }
        }
        
        switch method {
        case .GET:
            server.GET[remoteUrl] = response
        case .POST:
            server.POST[remoteUrl] = response
        }
    }
    
    func dataToJSON(data: Data) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
}

struct HTTPStubInfo {
    let url: String
    let jsonFilename: String
    let method: HTTPMethod
}

extension HTTPDynamicStubs {
    var testBundle: Bundle {
        return Bundle(for: type(of: self))
    }
    
    func applyStubForAssetListFetching(statusCode code: Int) {
        let remoteUrl = "/1/coding_test/13ZZQX/full"
        switch code {
        case 200:
            if let fileUrl = testBundle.url(forResource: "sample-assetlist-success", withExtension: "json") {
                setupStub(remoteUrl: remoteUrl, fileUrl: fileUrl, contentType: "application/json", method: .GET)
                return
            }
        default:
            server.GET[remoteUrl] = { _ in
                return HttpResponse.internalServerError
            }
            return
        }
    }
    
    func applyStubForImageFetching(imageRemoteUrl remoteUrl: String, filename: String = "", extension ext: String = "jpg", statusCode code: Int = 200) {
        switch code {
        case 200:
            if let fileUrl = testBundle.url(forResource: filename, withExtension: ext) {
                setupStub(remoteUrl: remoteUrl, fileUrl: fileUrl, contentType: "image/jpeg", method: .GET)
                return
            }
        default:
            server.GET[remoteUrl] = { _ in
                return HttpResponse.internalServerError
            }
            return
        }
    }
}
